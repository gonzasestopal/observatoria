function resumenChart(data) {
  
  ejes = []
  var idx = 0;

  for (ie of data.items) {
    if (data.promedios) {
      ejes.push(data.promedios[idx])
      idx++
    }
    for (eje of ie.elementos) {
      ejes.push(eje)
    }

  }
  
  // [{Eje}, {Eje}, {Eje}, {Eje}] Ordenados por universidad...

  var table = 
    d3.selectAll('.chart')
      .data(ejes)
      .append('svg')
        .attr('width', 52)
        .attr('height', 52)
        .classed("bolita", true)
      
  table.append("circle")
    .attr('cy', 26)
    .attr('cx', 26)
    // .attr('cx', (d, i)=> (i+1) * 50) d = data; i = index y jala las bolitas en el eje x hacia la derecha 50px cada vez
    .attr('r', (d) => d.valor * 5)
    .attr('fill', (d) => d.color ? d.color : d.indicador__eje__color)

  table.append("circle")
    .attr('cy', 26)
    .attr('cx', 26)
    .attr('r', 25)
    .attr('fill', "none")
    .attr('stroke', '#9c9c9c')
  table.append("texta")
    .attr('y', 31)
    .attr('x', 26)
    .text( (d) => d.valor)
    .style('font-size', '14px')
    .attr('text-anchor', 'middle')
    .style('font-weight', 'bold')
    .attr('matTooltip',"HI")
    //.style('fill', 'white')
    .style('fill', (function(d){
      if (d.valor > 1.5){
        the_color = d.color
        var r = parseInt(the_color.slice(1, 3), 16),
            g = parseInt(the_color.slice(3, 5), 16),
            b = parseInt(the_color.slice(5, 7), 16);
            return (r * 0.299 + g * 0.587 + b * 0.114) > 180
                ? '#000000'
                : '#FFFFFF';
        }
      else{
        return "#00000"
      }
      }
    ))




}