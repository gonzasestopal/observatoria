function heatmapChart(data) {
  
  console.log(data)
  
  var colors = d3.scaleLinear()
    .domain([0, 4.9, 5.1, 10])
    .range(["#8AF02B", "#152205","#211b02", "#FA6200"]);
  
  ejes = []
  for (ie of data) {
    for (eje of ie.elementos) {
      ejes.push(eje)
    }
  }
  
  // [{Eje}, {Eje}, {Eje}, {Eje}] Ordenados por universidad...

  var table = 
    d3.selectAll('.chart')
      .data(ejes)
      .append('svg')
        .attr('width', 300)
        .attr('height', 50)
        .classed("heat", true);
      
  table.append("rect")
    // .attr('x', 10)
    // .attr('y', 10)
    // .attr('cx', (d, i)=> (i+1) * 50) d = data; i = index y jala las bolitas en el eje x hacia la derecha 50px cada vez
    .attr('width', (d) => 300)
    .attr('height', 50)
    .attr('fill', (d) => colors(d.femin * 10))
      

}
