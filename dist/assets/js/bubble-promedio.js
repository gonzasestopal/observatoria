function promedioChart(data) {
  // d3.selectAll("svg").remove()
  
  var w = 150;
  var h = 150;

  var table = 
    d3.selectAll('.promedio-chart')
      .data(data)
      .append('svg')
        .attr('width', w)
        .attr('height', h)
      .append('g')
      .attr('transform', "translate(75,50)")
      
  table.append("circle")
    // .attr('cx', (d, i)=> (i+1) * 50) d = data; i = index y jala las bolitas en el eje x hacia la derecha 50px cada vez
    .attr('r', (d) => d.valor * 5)
    .attr('fill', (d) => d.color)
    
  table.append("circle")
    .attr('r', 25)
    .attr('fill', "none")
    .attr('stroke', 'gray')

  table.append("text")
    .attr('y', 40)
    .text( (d) => d.nombre_corto)
    .style('font-size', '10px')
    .attr('text-anchor', 'middle')
    .attr('font-weight', 'bold')
  
  table.append("text")
    .attr('y', 5)
    .text( (d) => d.valor)
    .style('font-size', '10px')
    .attr('text-anchor', 'middle')
    .style('font-weight', 'bold')
    .style('fill', 'white')

}