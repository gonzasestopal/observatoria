function mekkoChart(data, chart, data_genero) {

  var width = 450,
    height = 500,
    margin = {top: 110};

  chart_class = "." + chart

  var filtrado = data.filter(function(d){
    return d.tipo == chart && d.mekko == true
  })

  var filtrado2 = data_genero.filter(function(d){
    return d.tipo == chart && d.mekko == true
  })


  var nest = d3.nest()
    .key(function(d) {
      return d.segmento__nombre + d.nombre_grupo;
    })
    .entries(filtrado)

  var nest2 = d3.nest()
    .key(function(d) {
      return d.segmento__nombre + d.nombre_grupo;
    })
    .entries(filtrado2)


  var nestGrupo = d3.nest()
    .key(function(d) {
      return d.nombre_grupo;
    })
    .rollup(function(leaves){
      return d3.sum(leaves, function(d){
        var total = d.hombres + d.mujeres
        return total
      })
    })
    .entries(filtrado)

  console.log(nestGrupo)

  var gruposOrdenados = nestGrupo  
    .sort(function(a, b){ return d3.descending(a.value, b.value); })

  var double_mekko = height;
  var heightSVG = height
  if (gruposOrdenados.length > 1)
    double_mekko = height + ( height *  (gruposOrdenados[1].value / gruposOrdenados[0].value)),
    heightSVG =  double_mekko + 80;

  var treemapLayout = d3.treemap()
    .size([double_mekko, width])
    .round(true)
    .tile(d3.treemapSliceDice)
    .paddingTop(0)
    .padding(0)
    .paddingInner(1)

  var rootNode = d3.hierarchy({values: nest2}, function(d) { 
    return d.values
  })

  rootNode.sum(function(d) {
    return d.valor
  });


  treemapLayout(rootNode);

  try{
    var nombre_grupo = filtrado[0].nombre_grupo;
  }
  catch{
    var nombre_grupo = "";
  }
  

  var nodes = d3.select(chart_class)
    .append("svg")
      .attr("width", width + 250)
      .attr("height", heightSVG + margin.top + 10)
      .append("g")
      .selectAll('g.cell')
      .data(rootNode.descendants())
      .enter()
        .append('g')
        .attr('class', 'cell')
        .filter(function(d) {
          return d.depth > 1
        })
        .attr('transform', function(d) {
          if (d.data.nombre_grupo == nombre_grupo)
            return 'translate(' + [d.y0, d.x0 + margin.top] + ')';
          else
            return 'translate(' + [d.y0, d.x0 + margin.top + 90] + ')';
        })

  var nestGender = d3.nest()
    .key(function(d) {
      return d.nombre_grupo;
    })
    .rollup(function(leaves){
      return {
        "mujeres": d3.sum(leaves, function(d){
          return d.mujeres
        }),
        "hombres": d3.sum(leaves, function(d){
          return d.hombres
        })
      }
    })
    .entries(filtrado)




/*
  sums = d3.select(chart_class +" svg")
      .append("g")
      .selectAll("g.sum")
      .data (nestGender)
      .enter()
        .append("rect")
        .style('fill',function(d){
          return d.key == 'M' ? "#39a935" : "#ff9323";
        })
        .attr('class','sum')
        .attr('y',65)
        .attr('x',function(d){
          return d.key == 'M' ? 0 : xScale(total - d.value);          
        })
        .attr('height', 5)
        .attr('width', function(d){
          return xScale(d.value)
        })*/

  var restaAbs = 0

  if (filtrado[0].nombre_grupo != nestGrupo[0].key)
    restaAbs = 1;
  
  if (nestGrupo.length > 1){  
    var total2 = d3.sum(filtrado, function(d){
      if (d.nombre_grupo  == nestGrupo[Math.abs(1-restaAbs)].key)
        var total = d.hombres + d.mujeres
        return total
    })
    var xScale2 = d3.scaleLinear()
        .domain([0,total2])
        .range([0,width])
  }

  var total = d3.sum(filtrado, function(d){
    if (d.nombre_grupo  == nestGrupo[Math.abs(0-restaAbs)].key)
      var total = d.hombres + d.mujeres
      return total
  })
  var xScale = d3.scaleLinear()
      .domain([0,total])
      .range([0,width])


  var sums = d3.select(chart_class +" svg")
      .selectAll("g.sum")
      .data(nestGender)
      .enter()
        .append("g")
        .attr('class','sum')
        .attr('transform', function(d) {
          if (d.key == nombre_grupo)
            return 'translate(' + [0, 0] + ')';
          else
            return 'translate(' + [0, heightSVG + 10] + ')';
        })


  var natural_order = true
  if (gruposOrdenados[0] != nestGrupo[0])
    natural_order = false
  
  created("hombres")
  created("mujeres")

  function created(sexo) {
    sums
      .append("rect")
      .style('fill',function(d){
        return sexo == 'mujeres' ? "#39a935" : "#ff9323";
      })
      .attr('y',function(d,i){
        if (i <= 1)
          return 65;
        else
          return double_mekko + 30 + margin.top;
      })
      .attr('x',function(d,i){
        var total_actual = d.value.hombres + d.value.mujeres

        if (i < 1){
          return sexo == 'mujeres' ? 0 : xScale(total_actual - d.value[sexo]);
        }
        else{
          return sexo == 'mujeres' ? 0 : xScale2(total_actual - d.value[sexo]);
        }
      })
      .attr('height', 5)
      .attr('width', function(d, i){
        if (i < 1)
          return xScale(d.value[sexo]);
        else
          return xScale2(d.value[sexo]);
      })

    sums
      .append("text")
      .style('fill',function(d){
        return sexo == 'mujeres' ? "#39a935" : "#ff9323";
      })
      .style("font-size",23)
      .attr('y',function(d, i){
        if (i <= 1)
          return 60
        else
          return double_mekko + 25 + margin.top;
      })
      .attr('x', function(d) {
        return sexo == 'mujeres' ? 5 : width - 5;
      })
      .attr("text-anchor", function(d){ 
        return sexo == 'mujeres' ? "start" : "end"
      })
      .text(function(d){
        return (d.value[sexo] / (d.value.hombres + d.value.mujeres) * 100).toFixed(1) + "%"
      })

    }


  sums
    .append('text')
    .attr('x', width + 5)
    .attr('y', function(d,i){
      if (i <=1)
        return 55
      else
        return double_mekko + 30 + margin.top
    })
    .text(function(d){
      return d.key//.substr(0,d.key.length-2)
    })
    .style('font-weight', 'bold')
    .style("font-size",22)

  sums
    .append('text')
    .attr('x', width + 5)
    .attr('y', 75)
    .text(function(d){
      return d.value.hombres + d.value.mujeres
    })
    .style('font-weight', 'bold')
    .attr('fill', 'gray')
    .style("font-size",22)

  sums
    .append('polygon')
    .attr('points',"0,0, 0,20, 10,30, 20,20,  20,0")
    .attr("fill", "#fcc24c" )
    .attr("transform", function(d,i){
       console.log(d)
        if (i < 1)
          return 'translate(' + (xScale(d.value.mujeres) -10) + ',32 )' ;
        else
          return 'translate(' + (xScale2(d.value.mujeres) -10) + ',32 )' ;
      }
    );

  d3.select(chart_class +" svg")
    .append('text')
    .attr('x', 5)
    .attr('y', 1)
    .text("MUJERES")
    .attr("dy", ".71em")
    .attr("fill","#39a935")
    .style('font-weight', 'bold')

  d3.select(chart_class +" svg")
    .append('text')
    .attr('x', width - 5)
    .attr('y', 1)
    .text("HOMBRES")
    .attr("dy", ".71em")
    .attr("fill","#ff9323")
    .attr("text-anchor", "end")
    .style('font-weight', 'bold')

       
  nodes
    .append('rect')
    .style('fill', function(d) {
      return d.data.sexo == 'mujeres' ? "#39a935" : "#ff9323";
    })
    .attr('height', function(d) { 
      return d.x1 - d.x0;
    })
    .attr('width', function(d) {
      return d.y1 - d.y0;
    })

  nodes
    .append('text')
    .attr('y', function(d) {
      var dy = ((d.x1-d.x0)/2);
      return dy;
    })
    .attr('x', function(d) {
      return d.data.sexo == 'mujeres' ? 5 : d.y1 - d.y0 - 5;
    })
    .attr("text-anchor", function(d){ 
      return d.data.sexo == 'mujeres' ? "start" : "end"
    })
    .attr("dy", ".35em")
    .text(function(d) {
      //if ((d.x1 - d.x0) >= 8)
        return Math.round(d.data.valor * 100 / d.parent.value) + '%';
    })
    .style('fill', '#fff')
    .style("font-size",15)
  
  nodes
    .append('text')
    .attr('y', function(d) {
      return ((d.x1-d.x0)/2)
    })
    .attr('x', width + 5)
    .attr("dy", ".35em")
    .text(function(d) {
      if (/*(d.x1 - d.x0) >= 10 &&*/ d.data.sexo == 'mujeres')
        return d.data.segmento__nombre;
    })
    .style('fill', '#000')
    .style('font-weight', 'bold')
    .style("font-size",16)
    .on("mouseover", function(d) {
      var xPosition = d3.event.pageX;
      var yPosition = d3.event.pageY;
      d3.select("#tooltip")
        .style("left", xPosition + "px")
        .style("top", yPosition + "px")
        //.select("#name_segmento")
        //.text(d.segmento__descripcion)
        .select("#value")
        .text(d.parent.value)
      d3.select("#tooltip")
        .style("opacity", 100);
      d3.select("#tooltip")
        .select("#name_segmento")
        .text(d.data.segmento__descripcion)
    })
    .on("mouseout", function() {
      d3.select("#tooltip")
        .transition()
        .duration(600)
        .style("opacity", 0);
    })

  d3.select(chart_class + " svg").append('line')
    .attr('x1', width/2)
    .attr('x2', width/2)
    .attr('y1', 20)
    .attr('y2', heightSVG + margin.top)
    .style('stroke-dasharray', ("4,5"))
    .style('stroke-width', 2)
    .style("stroke", "gray") 

  d3.select(chart_class +" svg")
    .append('text')
    .attr('x', width/2)
    .attr('y', 0)
    .text("50%")
    .attr("dy", ".71em")
    .attr("fill", "gray")
    .attr("text-anchor", "middle")
    .style("font-size",15)

  }