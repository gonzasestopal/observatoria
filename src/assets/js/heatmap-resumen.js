function heatmapChart(data) {
  
  console.log(data)
  
  var colors = d3.scaleLinear()
    .domain([0, 4.9, 5 , 5.1, 10])
    .range(["#8AF02B", "#152205", "black","#211b02", "#FA6200"]);
  


  var nest = d3.nest()
    .key(function(d) {
      return d.institucion__siglas;
    })
    .entries(data.poblaciones)

  console.log(nest)

  var nestGrupo = d3.nest()
    .key(function(d) {
      return d.institucion__siglas + " " + d.segmento__grupo_segmento__tipo;
    })
    .rollup(function(leaves){
      return {
        "mujeres": d3.sum(leaves, function(d){
          return d.mujeres
        }),
        "hombres": d3.sum(leaves, function(d){
          return d.hombres
        })
      }
    })
    .entries(data.poblaciones)

  console.log(nestGrupo)
  
  // [{Eje}, {Eje}, {Eje}, {Eje}] Ordenados por universidad...

  var table = 
    d3.selectAll('.chart')
      .data(nestGrupo)
      .append('svg')
        .attr('width', 300)
        .attr('height', 50)
        .classed("heat", true);
      
  table.append("rect")
    .attr('width', (d) => 300)
    .attr('height', 50)
    .attr('fill', function(d){
      var total_ambos = d.value.mujeres + d.value.hombres
      if (total_ambos >0){
        var femin = (d.value.hombres / total_ambos )* 10
        return colors( femin);
      }
      else
        return "transparent"
    })

            

      //(d) => colors( (d.mujers )* 10))
      

}
