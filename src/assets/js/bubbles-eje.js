function ejeChart(data) {
  // d3.selectAll("svg").remove()

  var w = 150;
  var h = 150;
  
  var nest = d3.nest()
    .key(function(d) {
      return d.eje;
    })
    .entries(data)

  var table = 
    d3.selectAll('.bubble-chart')
    .data(nest)
    .selectAll('circle')
      .data(function(d, i ) {
        return d.values
      })
      .enter()
      .append('svg')
        .attr('width', w)
        .attr('height', h)
      .append('g')
      .attr('transform', "translate(75,50)")
      
  table.append("circle")
    // .attr('cx', (d, i)=> (i+1) * 50) d = data; i = index y jala las bolitas en el eje x hacia la derecha 50px cada vez
    .attr('r', (d) => d.valor * 5)
    .attr('fill', (d) => d.color)
    
  table.append("circle")
    .attr('r', 25)
    .attr('fill', "none")
    .attr('stroke', 'gray')
  
  table.append("text")
    .each(function(d) {
      var arr2 = d.nombre
      var first_split = 0
      var last_split = 0;
      var i = 0
      var finish = false;
      while (finish ==  false){
        last_split = arr2.lastIndexOf(" ",first_split + 20)
        if (arr2.substr(first_split,30).length < 20){
          finish = true;
          text = arr2.substr(first_split,20);
        }
        else
          text = arr2.substr(first_split,(last_split-first_split));
        d3.select(this).append('tspan')
          .attr('y', 25)
          .attr('x', 0)
          .attr("dy", 14 + (i * 8))
          .text(text)
          .style('font-size', '10px')
          .attr('text-anchor', 'middle')

        first_split = last_split + 1
        /*if (true){
          break
        }*/
        i++
      }

    })

    table.append("text")
      .attr('y', 4)
      .text( (d) => d.valor)
      .style('font-size', '10px')
      .attr('text-anchor', 'middle')
      .style('font-weight', 'bold')
      .style('fill', 'white')

}