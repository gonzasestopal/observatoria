import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { FormControl, Validators, NgForm } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { AppService } from '../app.service';

@Component({
  selector: 'filter',
  templateUrl: './filter.component.html',
  styleUrls: [
    './filter.component.css'
  ],
})
export class FilterComponent {

  /*
  Navbar filter componenet
  */

  filteredIes: Observable<any[]>;
  iesFormControl: FormControl;

  ies: any[] = [
      {name:"BUAP"}, {name:"CIAD"}, {name:"COLPOS"}, {name:"COLEF"}, {name:"ECOSUR"}, {name:"COLMEX"}, {name:"IPN"}, {name:"ITCAMPECHE"}, {name:"ITCELAYA"}, {name:"ITJIQUILPAN"}, {name:"ITSONORA"}, {name:"ITVALLEDELYAQUI"}, {name:"TECNM"}, {name:"UABJO"}, {name:"UAA"}, {name:"UABC"}, {name:"UABCS"}, {name:"UACAM"}, {name:"CHAPINGO"}, {name:"UNACH"}, {name:"UACH"}, {name:"UACJ"}, {name:"UADEC"}, {name:"UG"}, {name:"UAGro"}, {name:"UAN"}, {name:"UANL"}, {name:"UAQ"}, {name:"UASL"}, {name:"UAS"}, {name:"UJAT"}, {name:"UAT"}, {name:"UATX"}, {name:"UADY"}, {name:"UAZ"}, {name:"UNACAR"}, {name:"UAEH"}, {name:"UAEMex"}, {name:"UAEM"}, {name:"UAIS"}, {name:"UAM"}, {name:"UdeC"}, {name:"UDG"}, {name:"UQROO"}, {name:"USON"}, {name:"IBERO"}, {name:"UMSNH"}, {name:"UNAM"}, {name:"UPN"}, {name:"UPN-COLIMA"}, {name:"UPN-GDL"}, {name:"UPSIN"}, {name:"UV"}
  ];

  @ViewChild('filter') filter;

    constructor(private router: Router, private appService: AppService) {
    this.iesFormControl = new FormControl();
    this.filteredIes = this.iesFormControl.valueChanges
        .startWith(null)
        .map(ie => ie ? this.filterIes(ie) : this.ies.slice());
  }


  filterIes(name: string) {
    return this.ies.filter(ie =>
      ie.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  selectIES(event: MatAutocompleteSelectedEvent) {
    this.router.navigate(['/ies', event.option.value])
    this.iesFormControl.reset()
    this.filter.nativeElement.blur()
  }

}
