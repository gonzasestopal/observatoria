import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'eje',
  templateUrl: './eje.component.html'
})
export class EjeComponent implements OnInit {

  /*
  Eje information Component
  */

  eje: any;

  constructor(private route: ActivatedRoute, private appService: AppService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.appService.getEje2(params['slug'])
          .subscribe(data => {
            console.log(data)
            this.eje = data.items})
    })
  }

}
