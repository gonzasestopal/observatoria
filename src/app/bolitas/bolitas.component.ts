import {Component, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {AppService} from '../app.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent'
import {Http} from '@angular/http';
import { url } from '../tool';


declare var resumenChart: any;
declare var heatmapChart: any;

@Component({
  selector: 'bolitas',
  templateUrl: './bolitas.component.html',
  styleUrls: [ './bolitas.component.css']
})
export class BolitasComponent {

  /*
  Circle graphs inside mat-table component
  */

  columns: any[];
  displayedColumns: any[];

  exampleDatabase: ExampleHttpDao | null;
  dataSource: ExampleDataSource | null;

  @ViewChild('filter') filter: ElementRef;

  constructor(private appService: AppService, private http: Http, private route: ActivatedRoute) {}

  algo: boolean = false;

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.exampleDatabase = new ExampleHttpDao(this.http, params['slug']);
      this.dataSource = new ExampleDataSource(this.exampleDatabase)
      this.exampleDatabase.getData()
        .subscribe(data => {
          this.columns = [
            { columnDef: 'siglas', header: 'siglas', cell: (row: any) => `${row.siglas}` },
          ]
          if (params['slug'] != 'igualdad' && params['slug'] != undefined) {
            this.columns.push({ columnDef:  params['slug'], header: params['slug'], cell: (row: any) => `${row.promedio}` })
          }
          console.log(data)
          console.log(this.dataSource)
          console.log(this.columns)
          data.elementos.forEach((elemento, index) => {
              if (params['slug'] == undefined) {
                this.algo = true
                this.columns.push({ columnDef: elemento.nombre, header: elemento.nombre_corto, cell: (row: any) => row.elementos[index] !== undefined ? `${row.elementos[index]}`: null});
              }
              else {
                this.columns.push({ columnDef: elemento.nombre, header: elemento.nombre, cell: (row: any) => row.elementos[index] !== undefined ? `${row.elementos[index]}`: null})
              }
          })
          console.log(this.columns)

          this.displayedColumns = this.columns.map(x => x.columnDef)
          setTimeout(() => {
              Observable.fromEvent(this.filter.nativeElement, 'keyup')
                .debounceTime(150)
                .distinctUntilChanged()
                .subscribe(() => {
                    if (!this.dataSource) { return; }
                    this.dataSource.filter = this.filter.nativeElement.value;
                });
          })
      })
    })
  }
}

export interface Api {
  items: IESData[];
  elementos: Elemento[];
}

export interface Elemento {
  nombre_corto: string;
  tipo: string;
  nombre: string;
}

export interface Eje {
  siglas: string,
  legislacion: string,
  corresponsabiliad: string,
}

export interface IESData {
  nombre: string,
  ejes: Eje[],
}



/** An example database that the data source uses to retrieve data for the table. */
export class ExampleHttpDao {
  dataChange: BehaviorSubject<IESData[]> = new BehaviorSubject<IESData[]>([]);
  get data(): IESData[] { return this.dataChange.value; }

  constructor(private http: Http, private nombre?: string) {}

  getData(filter?: string) {
    console.log(filter)
    return this.getIndicadores(filter);
  }

  createGraph(data: any) {
    if (this.nombre != 'igualdad') {
      new resumenChart(data);
      return;
    }
    console.log("holi")
    new heatmapChart(data)
    return;
  }

  getIndicadores(filter?: string): Observable<Api> {
    console.log(this.nombre)
    let href = url; //'http://127.0.0.1:8000/'
    if (this.nombre && this.nombre != 'igualdad') href += 'indicadores/' + this.nombre + '/';
    if (this.nombre == 'igualdad') href += 'poblaciones/'
    const requestUrl =
      `${href}?q=${filter}`;

    return this.http.get(requestUrl)
                    .map(response => response.json() as Api);
  }
}

export class ExampleDataSource extends DataSource<IESData> {
  isLoadingResults = false;
  _filterChange = new BehaviorSubject('');
  get filter(): string { return this._filterChange.value; }
  set filter(filter: string) { this._filterChange.next(filter); }

  constructor(private _exampleDatabase: ExampleHttpDao) {
    super();
  }

  ngAfterViewInit() {

    setTimeout(() => {
      this.isLoadingResults = false
    })
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<IESData[]> {
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._filterChange,
    ];


    return Observable.merge(...displayDataChanges)
      .startWith(null)
      .switchMap(() => {
        setTimeout(() => {
          this.isLoadingResults = true;
        })
        return this._exampleDatabase.getData(this.filter)
      })
      .map(data => {
        setTimeout(() => {
          this._exampleDatabase.createGraph(data)
        })
        this.isLoadingResults = false;
        return data.items
    })
    .catch(() => {
      this.isLoadingResults = false;
      return Observable.of([]);
    });
  }

  disconnect() {}
}
