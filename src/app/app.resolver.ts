import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AppService } from './app.service';
import 'rxjs/add/observable/of';

@Injectable()
export class DataResolver implements Resolve<any> {
  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return Observable.of({ res: 'I am data'});
  }
}

@Injectable()
export class InstitucionResolver implements Resolve<any> {
    constructor(private appService: AppService, private router: Router) { }
    
    resolve(route: ActivatedRouteSnapshot): Observable<any> | boolean {
        return this.appService.getInstitucion(route.paramMap.get('slug'))
    }
}
/**
 * An array of services to resolve routes with data.
 */
export const APP_RESOLVER_PROVIDERS = [
  DataResolver
];
