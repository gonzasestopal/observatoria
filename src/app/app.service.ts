import { Injectable } from '@angular/core';
import { Http, Request, RequestOptions, Headers } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { url } from './tool';


@Injectable()
export class AppService {

  /*
  API requests service
  */

  private _url = url;

  constructor(private http: Http) { }

  public getIES(): Observable<any> {

    /*
    Retrieves registered universities
    */

    return this.http
        .get(this._url)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getInstitucion(siglas: string) {

    /*
    Retrieves registered university by acronym.
    */

    return this.http
        .get(this._url + siglas + '/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getEje(siglas: string, nombre: string): Observable<any> {

    /*
    Retrieves ejen name information by university acronym and eje's name.
    */

    return this.http
        .get(this._url + 'ies/' + siglas + '/' + nombre + '/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getEje2(nombre: string): Observable<any> {

    /*
    Retrieves eje information from all universities.
    */

    return this.http
        .get(this._url + 'eje/' + nombre + '/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getIES2(): Observable<any> {

    /*
    Retrieves registered universities information.
    */

    return this.http
        .get(this._url + 'all_ies/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getIndicadores(eje: string): Observable<any> {

    /*
    Retrieves indicador information by eje name.
    */

    return this.http
        .get(this._url + 'indicadores/' + eje + '/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getPromedios(siglas: string): Observable<any> {

    /*
    Retrieves the average of all ejes.
    */

    return this.http
        .get(this._url + 'ies2/' + siglas + '/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getAreas(siglas: string, tipo: string): Observable<any> {

    /*
    Retrieves areas of university by type.
    */

    return this.http
        .get(this._url + 'areas/' + siglas + '/' + tipo + '/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getEjes(siglas: string): Observable<any> {

    /*
    Retrieves existent ejes of university by acronim.
    */

    return this.http
        .get(this._url + 'ejes/' + siglas + '/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getSegmentos(): Observable<any> {

    /*
    Retrieves universities population by segment.
    */

    return this.http
        .get(this._url + 'segmentos/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getPoblacion(): Observable<any> {

    /*
    Retrieves universities population sorted by genre and academic type.
    */

    return this.http
        .get(this._url + 'poblacion/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getDashboardIndicadores(user: string): Observable<any> {
    return this.http
        .get(this._url + 'dashboard_indicadores/' + user + '/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getDashboardPoblacion(user: string): Observable<any> {
    return this.http
        .get(this._url + 'dashboard_poblacion/' + user + '/')
        .map(res => res.json())
        .catch(this.handleError);
  }

  public postOpcionRespuesta(valor: number, indicador: number, user: string): Observable<any> {
    let body = {
    	"valor": valor,
      "indicador": indicador,
      "user": user
    }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http
        .post(this._url + 'dashboard_indicadores/' + user + '/', body, options)
        .map(res => res.json())
        .catch(this.handleError);
  }


  /**
   * ========================= Nuevo endPoints
   */

  public getTodo(iesId){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http
        .post(`${this._url}api/init_principal/`, { ies_id : iesId }, options)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getPrincipales(iesId){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http
        .post(`${this._url}api/init_principal/`, { ies_id : iesId }, options)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public postPrincipales(data:any){
    return this.http
        .post(`${this._url}api/principal/`, data)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getEjess(iesId){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http
        .post(`${this._url}api/ies_questions/`, { ies_id : iesId }, options)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public postEjes(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http
        .post(`${this._url}api/ies_respuestas/`, data, options)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public postFilesQuestion(data:any){
    return this.http
        .post(`${this._url}api/ies_archivo/`, data)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public deleteFilesQuestion(data:any){
    return this.http
        .post(`${this._url}api/ies_archivo_delete/`, data)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getPoblaciones(iesId){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http
        .post(`${this._url}api/ies_poblation/`, { ies_id : iesId }, options)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public postPoblaciones(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http
        .post(`${this._url}api/poblacion_respuestas/`, data, options)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public getOtros(iesId){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http
        .post(`${this._url}api/ies_otros/`, { ies_id : iesId }, options)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public postOtros(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http
        .post(`${this._url}api/otros_respuestas/`, data, options)
        .map(res => res.json())
        .catch(this.handleError);
  }

  private handleError(error: Response) {
    console.log('An error ocurred ' + error);
    return Observable.throw(error || '500 internal server error');
  }

}
