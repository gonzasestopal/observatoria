import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';
import { IESComponent } from './ies';
import { EjeComponent } from './eje';
import { LoginComponent } from './login';
import { DashboardComponent } from './dashboard';
import { AuthGuardService as AuthGuard } from './auth.guard.service';

import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: '',      component: HomeComponent },
  //{ path: '',      component: LoginComponent },
  { path: 'home',  component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'eje/:slug', component: EjeComponent },
  { path: 'ies/:slug',   component: IESComponent },
  { path: '**',    component: NoContentComponent },
];
