import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { url } from './tool';

@Injectable()
export class AuthService {

  /*
  API auth service
  */

  private _url = url;

  private headers: Headers = new Headers({'Content-Type': 'application/json'});
  constructor(private http: Http) {}

  login(username, password): Observable<any> {
    let body = {
      'username' : username,
    	"password": password,
    }
    let headers = new Headers({ 'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    return this.http
        .post(this._url + 'user/login/', body, options)
        .map(res => res.json())
        .catch(this.handleError);
  }

  isAuthenticated(): boolean {
    return localStorage.getItem('user') !== null ? true : false;
  }

  getUser(): any {
    return localStorage.getItem('user')
  }

  getIesId() : any {
    return localStorage.getItem('user_ies_id');
  }

  private handleError(error: Response) {
    console.log('An error ocurred ' + error);
    return Observable.throw(error || '500 internal server error');
  }
}
