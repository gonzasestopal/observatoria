import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AppService } from '../app.service';
import { AuthService } from '../auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { DialogComponent } from './dialog/dialog.component';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css',]
})
export class DashboardComponent implements OnInit {

  /*
  Institution dashboard Component
  */

  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('logoInput') logoInput: ElementRef;
  @ViewChild('organigramaInput') organigramaInput: ElementRef;

  filesToUpload: Array<File> = [];

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  threeFormGroup: FormGroup;
  fourFormGroup: FormGroup;

  iesId;

  selected : number = 0;

  logoFiles : any;
  organigramaFiles : any;

  ejes : any = [];

  poblaciones : any = [];

  status : boolean;
  localPrincipal:any =  [];
  localEjes:any=[];
  localPoblaciones:any=[];
  localOtros:any=[];

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private _formBuilder: FormBuilder,
    private appService: AppService,
    private authService: AuthService) {
      let status = localStorage.getItem('status');
      this.status = JSON.parse(status);

      let principal = localStorage.getItem('principal');
      this.localPrincipal = JSON.parse(principal);

      let ejes = localStorage.getItem('ejes');
      this.localEjes = JSON.parse(ejes);

      let poblaciones = localStorage.getItem('poblaciones');
      this.localPoblaciones = JSON.parse(poblaciones);

      let otros = localStorage.getItem('otros');
      this.localOtros = JSON.parse(otros);
    }

  ngOnInit() {

    this.iesId = this.authService.getIesId();

    if(this.status){
      this.formPrincipales();
      this.getPrincipales(this.localPrincipal);

      this.formEjes(this.localEjes);
      this.getEjes(this.localEjes);

      this.formPoblaciones(this.localPoblaciones);
      this.getPoblaciones(this.localPoblaciones);

      this.formOtros();
      this.getOtros(this.localOtros);
    }else {
      this.snackBar.open('No se encontro datos para cargar en el formulario.', '', {
        duration: 3000
      });
    }

  }

  formPrincipales(){
    this.firstFormGroup = this._formBuilder.group({
      nombre : ['', Validators.required],
      siglas : ['', Validators.required],
      logo : '',
      ent_acad : ['', Validators.required],
      dep_admin : ['', Validators.required],
      planes_est : ['', Validators.required],
      organigrama : '',
    });
  }

  getPrincipales(principal:any){
    this.firstFormGroup.patchValue(principal);
    this.logoFiles = principal.logo;
    this.organigramaFiles = principal.organigrama;
  }

  addFilePrincipales(event:any, name:string){
    if(event.target.files.length > 0 && name == 'logo') {
      let file = event.target.files[0];
      this.logoFiles = file;
      this.firstFormGroup.get(name).setValue(file);
    }else{
      let file = event.target.files[0];
      this.organigramaFiles = file;
      this.firstFormGroup.get(name).setValue(file);
    }
  }

  prepareSavePrincipales() : any {
    let data = new FormData();

    data.append('ies_id', this.iesId);
    data.append('id', '');
    data.append('nombre', this.firstFormGroup.get('nombre').value);
    data.append('siglas', this.firstFormGroup.get('siglas').value);
    data.append('logo', this.firstFormGroup.get('logo').value);
    data.append('ent_acad', this.firstFormGroup.get('ent_acad').value);
    data.append('dep_admin', this.firstFormGroup.get('dep_admin').value);
    data.append('planes_est', this.firstFormGroup.get('planes_est').value);
    data.append('organigrama', this.firstFormGroup.get('organigrama').value);

    return data;
  }

  savePrincipales(){
    const data = this.prepareSavePrincipales();

    this.appService.postPrincipales(data).subscribe(response => {
      if(response.status){
        localStorage.removeItem('principal');
        localStorage.setItem('principal', JSON.stringify(response.principal));

        this.firstFormGroup.patchValue(response.principal);
        this.logoFiles = response.principal.logo;
        this.organigramaFiles = response.principal.organigrama;

        localStorage.removeItem('ejes');
        localStorage.setItem('ejes', JSON.stringify(response.ejes));
        this.ejes = response.ejes;

        this.snackBar.open('Datos guardados.', '', {
          duration: 3000
        });
        this.selected = 1;
      }else {
        this.snackBar.open('Ocurrio un problema al guardar los datos.', '', {
          duration: 3000
        });
        this.selected = 0;
      }
    })
  }

  formEjes(ejes:any){
    let questions = {};

      ejes.forEach(eje => {
        eje.indicadores.forEach(indicador => {
          indicador.questions.forEach(pregunta => {
            if(pregunta.options){
              questions = Object.assign(questions, {[pregunta.control] : ['', Validators.required]});
            }else{
              pregunta.instancias.forEach(instancia => {
                questions = Object.assign(questions, {[instancia.control] : ['', Validators.required]});
              });
            }
          });
        });
      });

      this.ejes = ejes;

      this.secondFormGroup = this._formBuilder.group(questions);
  }

  getEjes(ejes:any) {
      if(this.status){
        ejes.forEach(eje => {
          eje.indicadores.forEach(indicador => {
            indicador.questions.forEach(pregunta => {
              if(pregunta.options){
                this.secondFormGroup.get(pregunta.control).setValue(pregunta.value);
              }else{
                pregunta.instancias.forEach(instancia => {
                  this.secondFormGroup.get(instancia.control).setValue(instancia.value);
                });
              }
            });
          });
        });
      }else {
        this.ejes = [];
      }
  }

  addFileEjesQuestion(event: any, questionId:number) {
    this.filesToUpload = <Array<File>>event.target.files;

    const data: any = new FormData();
    const files: Array<File> = this.filesToUpload;

    for(let i =0; i < files.length; i++){
      data.append("ies_id", this.iesId);
      data.append("pregunta_id", questionId);
      data.append("files[]", files[i], files[i]['name']);
    }

    this.appService.postFilesQuestion(data).subscribe(response => {
      if(response.status){
        this.ejes.forEach(eje => {
          eje.indicadores.forEach(indicador => {
            indicador.questions.forEach(pregunta => {
              if(pregunta.id === questionId){
                let data = {
                  id : response.id,
                  name : response.name,
                  url : response.url
                };

                pregunta.files.push(data);
              }
            });
          });
        });
      }
    });
  }

  deleteFileEjesQuestion(questionId:number, file:any){
    this.appService.deleteFilesQuestion({ id : file.id}).subscribe(response => {
      if(response.status){
        this.ejes.forEach(eje => {
          eje.indicadores.forEach(indicador => {
            indicador.questions.forEach(pregunta => {
              if(pregunta.id === questionId){
                let index = pregunta.files.indexOf(file, 0);
                if (index > -1) {
                  pregunta.files.splice(index, 1);
                }
              }
            });
          });
        });
      }
    });
  }

  saveEjesQuestion(indicadorId:number) {
    const form : any = this.secondFormGroup.value;

    let questions: any;

    let questionT : any = {};
    let questionsT : any = [];

    let instanciaT : any = {};
    let instanciasT : any = [];

    this.ejes.forEach(eje => {
      eje.indicadores.forEach(indicador => {
        if(indicador.indicador_id === indicadorId){
          questions = indicador.questions;
        }
      });
    });

    questions.forEach(question => {
      if(question.options){
        questionT = Object.assign(questionT, { id : question.id, value : form[question.control] });
      }else{
        question.instancias.forEach(instancia => {
          instanciaT = Object.assign(instanciaT, { cve : instancia.cve, value : form[instancia.control] } );
          instanciasT.push(instanciaT);
          instanciaT = {};
        });

        questionT = Object.assign(questionT, { id : question.id, instancias : instanciasT });
      }

      questionsT.push(questionT);
      questionT = {};

    });

    let data = {
      respuestas : questionsT,
      ies_id : this.iesId
    };

    this.appService.postEjes(data).subscribe(response => {
      if(response.status){
        localStorage.removeItem('ejes');
        localStorage.setItem('ejes', JSON.stringify(response.ejes));
        this.ejes = response.ejes;

        this.snackBar.open('Datos guardados.', '', {
          duration: 3000
        });
      }else{
        this.snackBar.open('Ocurrio un problema al guardar los datos.', '', {
          duration: 3000
        });
      }
    })
  }

  formPoblaciones(poblaciones:any){
    let poblation = {};

    poblaciones.forEach(poblacion => {
      poblacion.grupos_segmento.forEach(grupo => {
        grupo.segmentos.forEach(segmento => {
          poblation = Object.assign(poblation, { [`hombres_${segmento.control}`] : '' });
          poblation = Object.assign(poblation, { [`mujeres_${segmento.control}`] : '' });
          poblation = Object.assign(poblation, { [`nombre_${segmento.control}`] : '' });
          poblation = Object.assign(poblation, { [`sin_informacion_${segmento.control}`] : '' });
          poblation = Object.assign(poblation, { [`no_aplica_${segmento.control}`] : '' });
        });
      });
    });

    this.poblaciones = poblaciones;

    this.threeFormGroup = this._formBuilder.group(poblation);
  }

  getPoblaciones(poblaciones:any){
    poblaciones.forEach(poblacion => {
      poblacion.grupos_segmento.forEach(grupo => {
        grupo.segmentos.forEach(segmento => {
          this.threeFormGroup.get(`hombres_${segmento.control}`).setValue(segmento.hombres);
          this.threeFormGroup.get(`mujeres_${segmento.control}`).setValue(segmento.mujeres);
          this.threeFormGroup.get(`sin_informacion_${segmento.control}`).setValue(segmento.sin_informacion);
          this.threeFormGroup.get(`nombre_${segmento.control}`).setValue(segmento.nombre);
          this.threeFormGroup.get(`no_aplica_${segmento.control}`).setValue(segmento.no_aplica);
        });
      });
    });
  }

  savePoblaciones(poblaciones){
    let array = [];

    poblaciones.segmentos.forEach(segmento => {
      let hombres = this.threeFormGroup.get(`hombres_${segmento.control}`).value;
      let mujeres = this.threeFormGroup.get(`mujeres_${segmento.control}`).value;
      let sin_informacion = this.threeFormGroup.get(`sin_informacion_${segmento.control}`).value;
      let nombre = this.threeFormGroup.get(`nombre_${segmento.control}`).value;
      let no_aplica = this.threeFormGroup.get(`no_aplica_${segmento.control}`).value;

      let data = {
        hombres : hombres,
        mujeres : mujeres,
        sin_informacion : sin_informacion,
        nombre : nombre,
        no_aplica : no_aplica
      };

      let seg = {
        segmento_id : segmento.segmento_id,
        segmento : data
      };

      array.push(seg);
    });

    let data = {
      respuestas : array,
      ies_id : this.iesId
    };

    this.appService.postPoblaciones(data).subscribe(response => {
      if(response.status){
        localStorage.removeItem('poblaciones');
        localStorage.setItem('poblaciones', JSON.stringify(response.poblaciones));
        this.poblaciones = response.poblaciones;
        this.snackBar.open('Datos guardados.', '', {
          duration: 3000
        });
      }else{
        this.snackBar.open('Ocurrio un problema al guardar los datos.', '', {
          duration: 3000
        });
      }
    });

  }

  formOtros(){
    this.fourFormGroup = this._formBuilder.group({
      ies_id : this.iesId,
      lactancia: '',
      guarderias: '',
      licencias_paternidad: '',
      licencias_maternidad: '',
      pob_lgbtiq_est: '',
      pob_lgbtiq_acad: '',
      pob_lgbtiq_admin: '',
      quejas_genero_hom: '',
      quejas_genero_muj: '',
      quejas_genero_lgbtiq: '',
      prev_violencia_muj: '',
      prev_violencia_hom: '',
      prev_violencia_lgbtiq: '',
      hombres_directores: '',
      mujeres_directoras: ''
    });
  }

  getOtros(otros:any){
    this.fourFormGroup.patchValue(otros);
  }

  saveOtros(){
    const data = this.fourFormGroup.value;
    this.appService.postOtros(data).subscribe(response => {
      if(response.status){
        localStorage.removeItem('otros');
        localStorage.setItem('otros', JSON.stringify(response.otros));

        this.snackBar.open('Datos guardados.', '', {
          duration: 3000
        });
      }else{
        this.snackBar.open('Ocurrio un problema al guardar los datos.', '', {
          duration: 3000
        });
      }
    });
  }

  verification(event) : void {
    if(event.index == 1){
      if (!this.firstFormGroup.valid){
        this.openDialog();
      }
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '450px',
      height: '100px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.selected = 0;
    });
  }
}
