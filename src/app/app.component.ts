/**
 * Angular 2 decorators and services
 */
import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';;
import { AuthService } from './auth.service';
import { Router, ActivatedRoute } from '@angular/router';

/**
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.css'
  ],
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  constructor(private auth: AuthService, private router: Router, private route: ActivatedRoute) { }

  ejes: any[];
  isAuthenticated: boolean;

  public ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
            this.isAuthenticated = params['isAuthenticated'] || this.auth.isAuthenticated();
      })

    this.ejes = [{'nombre':'Legislación','slug':'legislacion'}, {'nombre':'Igualdad','slug':'igualdad'},
    {'nombre':'Corresponsabilidad', 'slug':'corresponsabilidad'}, {'nombre':'Estadísticas','slug':'estadisticas'},
    {'nombre':'Lenguaje','slug':'lenguaje'}, {'nombre':'Sensibilización','slug':'sensibilizacion'}, {'nombre':'Estudios de Género','slug':'estudiosdegenero'},
    {'nombre':'Atención Violencia de Género','slug':'violencia'}]
  }

  public logout() {
    this.isAuthenticated = false;
    localStorage.removeItem('user')
    localStorage.removeItem('user_ies_id')
    localStorage.removeItem('status');
    localStorage.removeItem('principal');
    localStorage.removeItem('ejes');
    localStorage.removeItem('poblaciones');
    localStorage.removeItem('otros');

    this.router.navigate(['/login'])
  }

}
