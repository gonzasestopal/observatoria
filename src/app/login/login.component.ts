import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { FormControl, Validators } from '@angular/forms';
import { AppService } from '../app.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css',]
})
export class LoginComponent implements OnInit {

  /*
  Login Component
  */

  usernameFormControl = new FormControl('', [
    Validators.required,
  ])

  passwordFormControl = new FormControl('', [
    Validators.required
  ])

  constructor(
    public snackBar: MatSnackBar,
    private appService: AppService,
    private auth: AuthService,
    private router: Router) { }

  ngOnInit() {

  }

  login(username: string, password: string) {
    this.auth.login(username, password)
      .subscribe(data => {
        this.getTodo(data);
      }, err => {
        if (err.status == 404) {
          this.usernameFormControl.setErrors({'notFound': true})
          this.passwordFormControl.reset()
          this.snackBar.open('Error al iniciar sesión', '', {
            duration: 3000
          });
        }
      })
  }

  getTodo(data){
    this.appService.getTodo(data.ies_id).subscribe(response => {
      if(response.status){
        localStorage.setItem('user', data.user)
        localStorage.setItem('user_ies_id', data.ies_id)
        localStorage.setItem('status', JSON.stringify(response.status));
        localStorage.setItem('principal', JSON.stringify(response.principal));
        localStorage.setItem('ejes', JSON.stringify(response.ejes));
        localStorage.setItem('poblaciones', JSON.stringify(response.poblaciones));
        localStorage.setItem('otros', JSON.stringify(response.otros));

        this.router.navigate(['dashboard'], { queryParams: { isAuthenticated: true} })
      }else{
        this.snackBar.open('Error al cargar los datos del formulario', '', {
          duration: 3000
        });
        this.router.navigate(['login'])
      }
    }, error => {
      this.snackBar.open('Error al cargar los datos del formulario', '', {
        duration: 3000
      });
    });

  }

}
