import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
  NgModule,
  ApplicationRef
} from '@angular/core';
import {
  removeNgStyles,
  createNewHosts,
  createInputTransfer
} from '@angularclass/hmr';
import {
  RouterModule,
  PreloadAllModules
} from '@angular/router';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// Material 2
import { MaterialModule } from './material.module';
import { MatButtonModule } from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";
import 'hammerjs';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';
import { BolitasComponent } from './bolitas';
import { IESComponent } from './ies';
import { EjeComponent } from './eje';
import { LoginComponent } from './login';
import { FilterComponent } from './filter';
import { BarChartComponent } from './barchart';
import { SideNavComponent } from './sidenav';
import { DashboardComponent } from './dashboard';
import { XLargeDirective } from './home/x-large';
import { AppService } from './app.service';
import { AuthService } from './auth.service';
import { AuthGuardService } from './auth.guard.service';
import { DialogComponent } from './dashboard/dialog/dialog.component';

import '../styles/styles.scss';
import '../styles/headings.css';

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppService,
  AuthService,
  AuthGuardService,
];

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    AboutComponent,
    HomeComponent,
    NoContentComponent,
    BolitasComponent,
    IESComponent,
    EjeComponent,
    LoginComponent,
    FilterComponent,
    SideNavComponent,
    DashboardComponent,
    BarChartComponent,
    XLargeDirective,
    DialogComponent
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    BrowserModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES, {
      useHash: Boolean(history.pushState) === false,
      preloadingStrategy: PreloadAllModules
    }),
    MaterialModule
  ],
  entryComponents: [
    DialogComponent
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    ENV_PROVIDERS,
    APP_PROVIDERS
  ]
})
export class AppModule {

  constructor(
    public appRef: ApplicationRef,
  ) {}

}
