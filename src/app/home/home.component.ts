import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'home',
  styleUrls: [ './home.component.css' ],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  /*
  Homepage component
  */

  constructor(private router: Router) {

  }

  @Input('showMore') showMore:boolean;

  ngOnInit() { }

}
