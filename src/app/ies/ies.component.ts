import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../app.service';
import { forkJoin } from "rxjs/observable/forkJoin";

declare var iesChart: any;
declare var ejeChart: any
declare var mekkoChart: any;
declare var admChart: any;
declare var gaugeChart: any;
declare var gaugeChart2: any;
declare var promedioChart: any;

@Component({
  selector: 'ies',
  templateUrl: './ies.component.html',
  styleUrls: [ './ies.component.css']
})
export class IESComponent {

  /*
  University information component
  */

  UNAM: Boolean;
  ies: string;
  ejes: any
  items: any[];
  segmentos: any;
  poblaciones: any;
  total_estudiantes: number;

  objectKeys = Object.keys;

  constructor(private route: ActivatedRoute, private appService: AppService) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      if (params['slug'] === 'UNAM') this.UNAM = true;
      forkJoin([this.appService.getInstitucion(params['slug']),
                this.appService.getEjes(params['slug'])
                //this.appService.getPromedios(params['slug'])
              ])
        .subscribe(data => {
          console.log(data)
          new mekkoChart(data[0].poblaciones,"EST",data[0].poblaciones_genero);
          gaugeChart2(2.34, data[1].items);

          this.ejes = data[1].ejes;
          console.log(data[1].items)
          new ejeChart(data[1].alt);
          new promedioChart(data[1].items)

          this.ies = data[0];
          new iesChart(data[1].items);
          new mekkoChart(data[0].poblaciones,"ADM",data[0].poblaciones_genero)

          //gaugeChart.render(2.78)
        })
      // this.appService.getInstitucion(params['slug'])
      //   .subscribe(data => {
      //     this.ies = data.nombre
      //     new iesChart(data.items)
      //   })
      // this.appService.getEje(params['slug'], 'legislacion')
      //   .subscribe(data => new ejeChart(data.items))

    });
  }

}
