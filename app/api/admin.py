# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import IES, Eje, Respuesta, Indicador, Poblacion, Segmento, GrupoSegmento, Pregunta, Archivo

# Register your models here.
class IndicadorInLine(admin.TabularInline):
    model = Indicador
    extra = 2

class PreguntaInLine(admin.TabularInline):
    model = Pregunta
    extra = 2


class AdminEje(admin.ModelAdmin):
    model = Eje
    list_display = ('nombre', 'nombre_corto', 'descripcion', 'color')
    search_fields = ('nombre', 'nombre_corto', 'descripcion')
    inlines=[IndicadorInLine,]
admin.site.register(Eje, AdminEje)


class AdminIndicador(admin.ModelAdmin):
    model = Indicador
    list_display = ('nombre', 'color')
    search_fields = ('nombre', 'pregunta')
    list_filter   = ("eje",)
    inlines=[PreguntaInLine,]
admin.site.register(Indicador, AdminIndicador)


class PoblacionInLine(admin.TabularInline):
    model = Poblacion
    extra = 2

class AdminSegmento(admin.ModelAdmin):
    model = Segmento
    list_display = ('nombre', "descripcion", "grupo_segmento")
    search_fields = ('nombre', 'descripcion')
    inlines=[PoblacionInLine,]
admin.site.register(Segmento, AdminSegmento)
    

class SegmentoInLine(admin.TabularInline):
    model = Segmento
    extra = 1
class AdminGrupoSegmento(admin.ModelAdmin):
    model = GrupoSegmento
    list_display = ('nombre', 'tipo','orden')
    search_fields = ('nombre','tipo')
    inlines=[SegmentoInLine,]
admin.site.register(GrupoSegmento, AdminGrupoSegmento)

class AdminPoblacion(admin.ModelAdmin):
    model = Poblacion
    list_display = ('segmento', 'hombres', 'mujeres', 'institucion',)
    list_filter   = ("segmento", "institucion",)
admin.site.register(Poblacion, AdminPoblacion)

class Poblacion2InLine(admin.TabularInline):
    model = Poblacion
    extra = 0
class RespuestaInLine(admin.TabularInline):
    model = Respuesta
    extra = 0
class AdminIES(admin.ModelAdmin):
    model = IES
    list_display = ('nombre', 'siglas', 'user', "descripcion")
    search_fields   = ("nombre", "siglas", "descripcion")
    inlines=[Poblacion2InLine,RespuestaInLine,]
admin.site.register(IES, AdminIES)


admin.site.register(Respuesta)
admin.site.register(Archivo)
