from rest_framework.authtoken import views
from django.contrib.auth.models import User
from rest_framework import routers
from django.conf.urls import url, include


router = routers.DefaultRouter()
"""
router.register(r'settings', SettingsViewSet)
"""
from api.views import (MultiFiles, QuestionsIES, PoblationIES, JsonViewer, PrincipalesIES,
                ArchivoViewSet, PrincipalesIES, PrinciapesRespuestas, ArchivoIES,
                ArchivoIESDelete, AnswersIES, PoblationAnswersIES, UserLoginAPIView,
                OtrosIES, OtrosAnswersIES)
router.register(r'archivos', ArchivoViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),

    url(r'^login/$', UserLoginAPIView.as_view(), name='login'),

    url(r'^token-auth/', views.obtain_auth_token),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^test/$', MultiFiles.as_view(), name='test_multifield'),
    url(r'^json_analitic/$', JsonViewer.as_view(), name='Preguntas para IES'),

    url(r'^init_principal/$', PrincipalesIES.as_view(), name='Inical Principal para IES'),
    url(r'^principal/$', PrinciapesRespuestas.as_view(), name='Principales para IES'),

    url(r'^ies_questions/$', QuestionsIES.as_view(), name='Preguntas para IES'),
    url(r'^ies_archivo/$', ArchivoIES.as_view(), name='Archivo para IES'),
    url(r'^ies_archivo_delete/$', ArchivoIESDelete.as_view(), name='Eliminacion de Archivo'),
    url(r'^ies_respuestas/$', AnswersIES.as_view(), name='Respuestas para IES'),

    url(r'^ies_poblation/$', PoblationIES.as_view(), name='Poblaciones para IES'),
    url(r'^poblacion_respuestas/$', PoblationAnswersIES.as_view(), name='Respuestas de Poblaciones'),


    url(r'^ies_otros/$', OtrosIES.as_view(), name='Poblaciones para IES'),
    url(r'^otros_respuestas/$', OtrosAnswersIES.as_view(), name='Respuestas de Poblaciones'),

]
