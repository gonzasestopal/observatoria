from rest_framework import serializers
from .models import Archivo, IES
from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator
from rest_framework.authtoken.models import Token


class FileSerializer(serializers.Serializer):
    images=serializers.ListField(child=serializers.FileField(allow_empty_file=True),
                                write_only=True)

class idSerializer(serializers.Serializer):
    ies_id=serializers.IntegerField(write_only=True)


class ArchivoSerializer(serializers.ModelSerializer):
    name = serializers.CharField(allow_blank=True, read_only=True)
    status = serializers.BooleanField(read_only=True)
    class Meta:
        model = Archivo
        fields = ('id', 'ies', 'pregunta', 'doc_respaldo', 'name', 'status')
    def validate(self, data):
        import os
        doc_respaldo = data.get('doc_respaldo', None)
        if doc_respaldo:
            data['name'] = os.path.basename(doc_respaldo.name)
            data['status'] = True

        return data

class IESSerializer(serializers.ModelSerializer):
    class Meta:
        model = IES
        fields = ("lactancia", "guarderias", "licencias_paternidad", "licencias_maternidad",
        "pob_lgbtiq_est", "pob_lgbtiq_acad", "pob_lgbtiq_admin", "quejas_genero_hom",
        "quejas_genero_muj", "quejas_genero_lgbtiq", "prev_violencia_muj",
        "prev_violencia_hom", "prev_violencia_lgbtiq", "hombres_directores",
        "mujeres_directoras")

class UserLoginSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True, write_only=True,
            style={'input_type': 'password'})

    token = serializers.CharField(allow_blank=True, read_only=True)
    ies_id = serializers.CharField(allow_blank=True, read_only=True)

    class Meta(object):
        model = User
        fields = ['username', 'password', 'token', 'ies_id']

    def validate(self, data):
        username = data.get('username', None)
        password = data.get('password', None)

        if username:
            try:
                user = User.objects.get(username=username)
            except Exception as e:
                raise serializers.ValidationError("This username/email is not valid.")
        else:
            user=False
            raise serializers.ValidationError("Please enter username to login.")

        if user:
            if not user.check_password(password):
                raise serializers.ValidationError("Invalid credentials.")

        if user.is_active:
            token, created = Token.objects.get_or_create(user=user)
            data['token'] = token
            try:
                ies=IES.objects.get(user=user)
                data['ies_id'] = ies.id
            except Exception as e:
                pass
        else:
            raise serializers.ValidationError("User not active.")

        return data
