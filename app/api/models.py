# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from django.conf import settings
from django.db import models
from django.db.models import Avg, F
from django.contrib.auth.models import User

# Create your models here.
class IES(models.Model):
    user = models.OneToOneField(User)
    nombre = models.CharField(max_length=255,help_text=u"Nombre completo")
    logo = models.ImageField(upload_to="ies")
    siglas = models.CharField(max_length=50,help_text=u"Siglas únicas")
    descripcion = models.TextField(null=True,blank=True)
    #entidades académicas
    ent_acad = models.IntegerField(null=True,blank=True,help_text=u"Entidades académicas")
    #dependencias administrativas
    dep_admin = models.IntegerField(null=True,blank=True,help_text=u"dependencias administrativas")
    #planes de estudio licenciaturas y posgrados
    planes_est = models.IntegerField(null=True,blank=True,help_text=u"Planes de estudio")
    organigrama = models.FileField(blank=True,null=True)
    lactancia = models.IntegerField(null=True,blank=True)
    guarderias = models.IntegerField(null=True,blank=True)
    licencias_paternidad = models.IntegerField(null=True,blank=True)
    licencias_maternidad = models.IntegerField(null=True,blank=True)
    pob_lgbtiq_est = models.IntegerField(null=True,blank=True)
    pob_lgbtiq_acad = models.IntegerField(null=True,blank=True)
    pob_lgbtiq_admin = models.IntegerField(null=True,blank=True)
    quejas_genero_hom = models.IntegerField(null=True,blank=True)
    quejas_genero_muj = models.IntegerField(null=True,blank=True)
    quejas_genero_lgbtiq = models.IntegerField(null=True,blank=True)
    prev_violencia_muj = models.IntegerField(null=True,blank=True)
    prev_violencia_hom = models.IntegerField(null=True,blank=True)
    prev_violencia_lgbtiq = models.IntegerField(null=True,blank=True)
    hombres_directores = models.IntegerField(null=True,blank=True)
    mujeres_directoras = models.IntegerField(null=True,blank=True)

    def __unicode__(self):
        return self.siglas

    def logo_img(self):
        return "{0}{1}".format(settings.MEDIA_URL, self.logo.url)
    
    def ejes(self):
        return Respuesta.objects.filter(ies=self) \
                .values('pregunta__indicador__eje__nombre_corto', 'pregunta__indicador__eje__nombre', 'valor', 'pregunta__indicador__eje__color' )
    
    def poblaciones(self):
        return Poblacion.objects.filter(institucion=self).exclude(segmento__nombre__icontains='Total')\
            .annotate(nombre_grupo=F('segmento__grupo_segmento__nombre'),
                orden_grupo=F('segmento__grupo_segmento__orden'),
                tipo=F('segmento__grupo_segmento__tipo'),
                mekko=F('segmento__grupo_segmento__mekko'), )\
            .values('segmento__nombre','segmento__id', 'hombres', 'mujeres', 'tipo',
                'segmento__descripcion','nombre_grupo', 'orden_grupo', 'mekko')\
            .order_by('tipo','orden_grupo','segmento__id')
    class Meta:
        verbose_name_plural = 'Institución'

class GrupoSegmento(models.Model):
    POBLACION_CHOICES = (
        ('ADM', u'Personal Administrativo'),
        ('ACD', u'Personal Académico'),
        ('EST', u'Población Estudiantil')
    )

    nombre = models.CharField(max_length=120)
    tipo = models.CharField(max_length=3, choices=POBLACION_CHOICES)
    mekko = models.BooleanField(help_text=u"¿El grupo-segmento se grafica con mekko?")
    orden = models.IntegerField(help_text=u"Órden en el que deberán aparecer")
    pregunta = models.TextField(default="Indique a continuación el número de personas que forman parte de cada lo indicado")
    
    def __unicode__(self):
        return self.nombre + ' ' + self.tipo

class Segmento(models.Model):

    nombre = models.CharField(max_length=120)
    descripcion = models.CharField(max_length=120,blank=True,null=True)
    grupo_segmento = models.ForeignKey('GrupoSegmento')
    punto_divisor = models.BooleanField(default=False, help_text=u"Para la gráfica de barras horizontal")
    is_total = models.BooleanField(default=False, help_text=u"¿Se trata del total del Grupo Segmento?")
    is_name_requiered = models.BooleanField(default=False, help_text=u"¿Requiere el valor del nombre?")
    
    def __unicode__(self):
        return self.nombre + '-' +self.grupo_segmento.nombre


class Poblacion(models.Model):
    segmento = models.ForeignKey('Segmento', default=0)
    nombre = models.CharField(max_length=255, blank=True, null=True)
    hombres = models.IntegerField(blank=True, null=True)
    mujeres = models.IntegerField(blank=True, null=True)
    institucion = models.ForeignKey(IES)
    sin_informacion = models.BooleanField(default=False)
    no_aplica = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s %s -- Hombres: %s, Mujeres: %s"%(
            self.segmento.nombre, self.institucion.siglas, self.hombres, self.mujeres)

class Eje(models.Model):
    nombre = models.CharField(max_length=255)
    nombre_corto = models.CharField(max_length=55)
    slug = models.CharField(max_length=55, blank=True, null=True)
    logo = models.ImageField(upload_to='eje')
    descripcion = models.TextField()
    color = models.CharField(max_length=55)
    
    def __unicode__(self):
        return self.nombre

class Indicador(models.Model):
    nombre = models.CharField(max_length=255)
    color = models.CharField(max_length=55)
    eje = models.ForeignKey('Eje')
    nota = models.TextField(blank=True,null=True)
    academia = models.NullBooleanField(help_text=u"Se toman en cuenta las entidades académicas")
    administ = models.NullBooleanField(help_text=u"Se toman en cuenta las dependencias administrativas")
    planes = models.NullBooleanField(help_text=u"Se toman en cuenta los planes de estudio")
    transver_value = models.IntegerField(default=3,help_text=u"Peso de Transversalidad de 5 puntos")
    instit_value = models.IntegerField(default=2,help_text=u"Peso de Insitucionalización de 5 puntos")

    class Meta:
        verbose_name_plural = 'Indicadores'
        
    def __unicode__(self):
        return self.nombre


class Pregunta(models.Model):
    text = models.TextField()
    archivo_probatorio = models.TextField(default="Archivo probatorio")
    indicador = models.ForeignKey('Indicador')
    opcion_no_aplica = models.BooleanField(default=False)
    #La siguiente variable indica la variabilidad de la pregunta si es de tipo 
    # Sí/No (True) o numérico para cada instancia (False)
    institut = models.BooleanField(default=True,help_text=u"Se trata de una pregunta de institucionalización")
        
    def __unicode__(self):
        return self.text


class Respuesta(models.Model):
    pregunta = models.ForeignKey('Pregunta')
    ies = models.ForeignKey('IES')
    valor = models.IntegerField(blank=True,null=True)
    academia = models.IntegerField(blank=True,null=True)
    administ = models.IntegerField(blank=True,null=True)
    planes = models.IntegerField(blank=True,null=True)
    anio = models.IntegerField(blank=True,null=True)
    
    def __unicode__(self):
        return u"%s %s"%(self.ies.nombre, self.pregunta.text)
"""    
    INSTANCIAS_CHOICES = (
        ('ACD', u'Instancias Académicas'),
        ('ADM', u'Dependencias Administrativas'),
        ('PLAN', u'Planes de Estudio')
    )
""" 

class Archivo(models.Model):
    ies = models.ForeignKey('IES')
    pregunta = models.ForeignKey('Pregunta')
    doc_respaldo = models.FileField()
    def __unicode__(self):
        return "IES: %s, Pregunta: %s - %s"%(
            self.ies.nombre, self.pregunta_id, self.doc_respaldo.name)


"""
class OpcionesRespuesta(models.Model):
    VALOR_CHOICES = [(i,i) for i in range(6)]
    indicador = models.ForeignKey('Indicador')
    texto = models.TextField()
    valor = models.IntegerField(choices=VALOR_CHOICES, blank=True, null=True)
    
    class Meta:
        verbose_name_plural = 'Opciones de Respuesta'
    
    def __unicode__(self):
        return self.texto
"""