# -*- coding: utf-8 -*-
from tastypie.resources import ModelResource
from rest_framework.response import Response
from api.models import IES, Archivo

from rest_framework import generics, permissions, viewsets, views, status
from api.serializers import (FileSerializer, idSerializer, ArchivoSerializer,
                UserLoginSerializer, IESSerializer)

from django.conf import settings
import os

from api.models import (Pregunta, IES, Respuesta, Indicador, Archivo,
                GrupoSegmento, Segmento, Poblacion)
m_host=settings.DOMINIO+settings.MEDIA_URL
from django.core.files.uploadedfile import InMemoryUploadedFile


class IESResource(ModelResource):
    class Meta:
        queryset = IES.objects.all()
        resource_name = 'ies'


class UserLoginAPIView(views.APIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = UserLoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class PoblationIES(views.APIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = idSerializer
    def post(self, request, *args, **kwargs):
        id_ies=request.data.get('ies_id')
        if not id_ies:
            return Response({"status":False}, status=status.HTTP_400_BAD_REQUEST)
        try:
            ies=IES.objects.get(id=id_ies)
        except Exception as e:
            ies=False
            return Response({"status":False, "error":"id de IES no encontrado"},
                status=status.HTTP_400_BAD_REQUEST)
        try:
            return Response({"status":True, "poblaciones":poblacionIES(ies)}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"status":False}, status=status.HTTP_400_BAD_REQUEST)

def poblacionIES(ies):
    claves={'ADM' : u'Personal Administrativo',
            'ACD' : u'Personal Académico',
            'EST' : u'Población Estudiantil'}

    poblaciones=[]

    for key, value in claves.iteritems():
        grupos_segmento=[]
        gr_seg=GrupoSegmento.objects.filter(tipo=key)
        for grupo in gr_seg:
            segmentos=[]
            segments=Segmento.objects.filter(grupo_segmento=grupo)
            for segmento in segments:
                poblacion, created=Poblacion.objects.get_or_create(
                    segmento=segmento, institucion=ies)
                seg_={
                    "control":"segmento%s"%segmento.id,
                    "segmento_id":segmento.id,
                    "nombre":segmento.nombre,
                    "descripcion":segmento.descripcion,
                    "hombres":poblacion.hombres,
                    "mujeres":poblacion.mujeres, 
                    "sin_informacion":poblacion.sin_informacion,
                    "no_aplica":poblacion.no_aplica
                }
                segmentos.append(seg_)
            grupos_segmento.append(
                {
                    "grupos_segmento_id":grupo.id,
                    "nombre":grupo.nombre,
                    "pregunta":grupo.pregunta,
                    "segmentos":segmentos
                })
        poblaciones.append({
                "nombre":value,
                "grupos_segmento":grupos_segmento
            })

    return poblaciones

class PoblationAnswersIES(views.APIView):
    permission_classes = (permissions.AllowAny, )
    def post(self, request, *args, **kwargs):
        
        respuestas=request.data.get('respuestas')
        ies_id=request.data.get('ies_id')


        for respuesta in respuestas:
            if "segmento_id" in respuesta:
                segmento_id=respuesta["segmento_id"]
            else:
                continue
            try:
                poblacion_obj=Poblacion.objects.get(segmento_id=segmento_id,
                    institucion_id=ies_id)
            except Exception as e:
                continue
            if "segmento" in respuesta:
                respuesta=respuesta["segmento"]
            else:
                continue
            try:
                hombres=int(respuesta["hombres"])
            except Exception as e:
                hombres=False
            try:
                mujeres=respuesta["mujeres"]
            except Exception as e:
                mujeres=False


            sin_informacion=respuesta["sin_informacion"] if "sin_informacion" in respuesta else False
            no_aplica=respuesta["no_aplica"] if "no_aplica" in respuesta else False

            if not sin_informacion and not no_aplica:
                if hombres:
                    poblacion_obj.hombres=hombres
                if mujeres:
                    poblacion_obj.mujeres=mujeres
                if respuesta["nombre"]:
                    poblacion_obj.nombre=respuesta["nombre"]

            if sin_informacion:
                poblacion_obj.sin_informacion=True
            if no_aplica:
                poblacion_obj.no_aplica=True

            poblacion_obj.save()

        return ies_data_responce(poblacion_obj.institucion)



#**************************Principal*********************
def ies_data_responce(ies):
    datos=IESSerializer(ies)
    api_datos={"status": True,
    "principal":principalesIES(ies),
    "ejes":questionsIES(ies),
    "poblaciones":poblacionIES(ies),
    "otros":datos.data}
    return Response(api_datos, status=status.HTTP_200_OK)

 
class PrincipalesIES(views.APIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = idSerializer
    def post(self, request, *args, **kwargs):
        id_ies=request.data.get('ies_id')
        if not id_ies:
            return Response({"status":False}, status=status.HTTP_400_BAD_REQUEST)
        try:
            ies=IES.objects.get(id=id_ies)
        except Exception as e:
            ies=False
            return Response({"status":False, "error":"id de IES no encontrado"},
                status=status.HTTP_400_BAD_REQUEST)

        return ies_data_responce(ies)

def principalesIES(ies):
    m_host=settings.DOMINIO+settings.MEDIA_URL

    principal={
        "id": ies.id,
        "nombre": ies.nombre,
        "siglas": ies.siglas,
        "logo": {"url":"%s%s"%(m_host, ies.logo.name),
                 "name":os.path.basename(ies.logo.name)
                } if ies.logo else False,
        "ent_acad": ies.ent_acad,
        "dep_admin": ies.dep_admin,
        "planes_est": ies.planes_est,
        "organigrama": {"url":"%s%s"%(m_host, ies.organigrama.name),
                 "name":os.path.basename(ies.organigrama.name)
                } if ies.organigrama else False
    }
    return principal

class PrinciapesRespuestas(views.APIView):
    permission_classes = (permissions.AllowAny, )
    def post(self, request, *args, **kwargs):
        id_ies=request.data.get('ies_id')
        ent_acad=request.data.get('ent_acad')
        planes_est=request.data.get('planes_est')
        organigrama=request.data.get('organigrama')
        logo=request.data.get('logo')
        nombre=request.data.get('nombre')
        dep_admin=request.data.get('dep_admin')
        siglas=request.data.get('siglas')
        if not id_ies:
            return Response({"status":False}, status=status.HTTP_400_BAD_REQUEST)
        try:
            ies=IES.objects.get(id=id_ies)
        except Exception as e:
            ies=False
            return Response({"status":False, "error":"id de IES no encontrado"},
                status=status.HTTP_400_BAD_REQUEST)
        try:
            if type(logo) is InMemoryUploadedFile:
                ies.logo=logo

            if type(organigrama) is InMemoryUploadedFile:
                ies.organigrama=organigrama
            ies.nombre=nombre
            ies.siglas=siglas
            ies.ent_acad=ent_acad
            ies.dep_admin=dep_admin
            ies.planes_est=planes_est
            ies.save()
        except Exception as e:
            return Response({"status":False, "error":e},
                status=status.HTTP_400_BAD_REQUEST)
        return ies_data_responce(ies) 

#**************************Componentes*******************
class QuestionsIES(views.APIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = idSerializer
    def post(self, request, *args, **kwargs):
        id_ies=request.data.get('ies_id')
        if not id_ies:
            return Response({"status":False}, status=status.HTTP_400_BAD_REQUEST)
        try:
            ies=IES.objects.get(id=id_ies)
        except Exception as e:
            ies=False
            return Response({"status":False, "error":"id de IES no encontrado"},
                status=status.HTTP_400_BAD_REQUEST)
        try:
            return ies_data_responce(ies)
        except Exception as e:
            return Response({"status":False}, status=status.HTTP_400_BAD_REQUEST)

def questionsIES(ies):
    preguntas=Pregunta.objects.all().order_by("indicador__eje", "indicador")
    eje=False
    indicador=False
    ejes=[]
    indicadores=[]
    questions=[]
    for pregunta in preguntas:
        if indicador != pregunta.indicador:
            if indicador:
                indicadores.append({
                    "nombre" : indicador.nombre,
                    "nota": indicador.nota,
                    "indicador_id" : indicador.id,
                    "questions" : questions
                    })
            indicador=pregunta.indicador
            questions=[]
        if eje != pregunta.indicador.eje:
            if eje:
                ejes.append({
                    "nombre": eje.nombre,
                    "indicadores":indicadores
                    })
            eje=pregunta.indicador.eje
            indicadores=[]
        respuesta, created=Respuesta.objects.get_or_create(pregunta=pregunta, ies=ies)
        if pregunta.institut:
            ###pregunta se si/no
            value=None
            if respuesta.valor != None:
                value="%s"%respuesta.valor
            ask={
                "id": pregunta.id,
                "text" : pregunta.text, 
                "options" : True, 
                "files" : [],
                "value": ("%s"%respuesta.valor) if respuesta.valor!= None else None,
                "archivo_probatorio": pregunta.archivo_probatorio,
                "control":"question%s"%pregunta.id
            }
        else:
            ask={
                "id": pregunta.id,
                "options" : False, 
                "files" : [],
                "archivo_probatorio": pregunta.archivo_probatorio,
                "instancias" : [],
            }
            if pregunta.indicador.academia:
                instancia={"cve":"academia", "value":respuesta.academia, "control":"academia%s"%pregunta.id,
                    "text":u"De las %s instancias académicas, %s"%(ies.ent_acad, pregunta.text)}
                ask["instancias"].append(instancia)
            if pregunta.indicador.administ:
                instancia={"cve":"administ", "value":respuesta.administ, "control":"administ%s"%pregunta.id,
                    "text":u"De las %s dependencias administrativas, %s"%(ies.dep_admin, pregunta.text)}
                ask["instancias"].append(instancia)
            if pregunta.indicador.planes:
                instancia={"cve":"planes", "value":respuesta.planes, "control":"planes%s"%pregunta.id,
                    "text":u"De los %s planes de estudio licenciaturas y posgrados, %s"%(ies.planes_est, pregunta.text)}
                ask["instancias"].append(instancia)
        files=Archivo.objects.filter(pregunta=pregunta, ies=ies)
        archivos=[]
        for file in files:
            archivos.append({
                "name":os.path.basename(file.doc_respaldo.name),
                "url":"%s%s"%(m_host, file.doc_respaldo.name),
                "id":file.id})
        ask["files"]=archivos

        questions.append(ask)

    if indicador:
        indicadores.append({
            "nombre" : indicador.nombre,
            "nota": indicador.nota,
            "questions" : questions
            })
    if eje:
        ejes.append({
            "nombre": eje.nombre,
            "indicadores":indicadores
            })
    return ejes

class ArchivoIES(views.APIView):
    permission_classes = (permissions.AllowAny, )
    def post(self, request, *args, **kwargs):
        ies_id=int (request.data.get('ies_id'))
        pregunta_id=request.data.get('pregunta_id')
        archivo=request.data.get('files[]')
        if type(ies_id) is int:
            pass
        elif type(ies_id) is unicode or type(ies_id) is str:
            ies_id=int(ies_id) if ies_id.isdigit() else False
        else:
            ies_id=False

        if type(pregunta_id) is int:
            pass
        elif type(pregunta_id) is unicode or type(pregunta_id) is str:
            pregunta_id=int(pregunta_id) if pregunta_id.isdigit() else False
        else:
            pregunta_id=False


        if type(ies_id) is int and type(pregunta_id) is int:
            try:
                archivo_ies=Archivo.objects.create(
                    ies_id=ies_id,
                    pregunta_id=pregunta_id,
                    doc_respaldo=archivo)
                return Response({
                    "status":True,
                    "name":os.path.basename(archivo_ies.doc_respaldo.name),
                    "url":"%s%s"%(m_host, archivo_ies.doc_respaldo.name),
                    "id":archivo_ies.id},
                    status=status.HTTP_200_OK)
            except Exception as e:
                return Response({"status":False, "error":e}, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response({"status":False}, status=status.HTTP_400_BAD_REQUEST)

class ArchivoIESDelete(views.APIView):
    permission_classes = (permissions.AllowAny, )
    def post(self, request, *args, **kwargs):
        archivo_id=request.data.get('id')
        if not archivo_id:
            return Response({"status":False}, status=status.HTTP_400_BAD_REQUEST)
        try:
            archivo=Archivo.objects.get(id=archivo_id)
        except Exception as e:
            archivo=False
            return Response({"status":False, "error":"id de Archivo no encontrado"},
                status=status.HTTP_400_BAD_REQUEST)
        archivo.delete()
        return Response({"status":True}, status=status.HTTP_200_OK)

class AnswersIES(views.APIView):
    permission_classes = (permissions.AllowAny, )
    def post(self, request, *args, **kwargs):        
        respuestas=request.data.get('respuestas')
        ies_id=request.data.get('ies_id')
        try:
            ies=IES.objects.get(id=ies_id)
        except Exception as e:
            ies=False
            return Response({"status":False, "error":"id de IES no encontrado"},
                status=status.HTTP_400_BAD_REQUEST)

        for respuesta in respuestas:
            pregunta_id=respuesta["id"]
            try:
                respuesta_obj=Respuesta.objects.get(pregunta_id=pregunta_id, ies_id=ies_id)
            except Exception as e:
                continue
            if "instancias" in respuesta:
                for instancia in respuesta["instancias"]:
                    if "cve" in instancia and "value" in instancia:
                        clave=instancia["cve"]
                        value=instancia["value"]
                        if clave=="academia":
                            respuesta_obj.academia=value
                        elif clave=="administ":
                            respuesta_obj.administ=value
                        elif clave=="planes":
                            respuesta_obj.planes=value
            else:
                if respuesta["value"].isdigit():
                    respuesta_obj.valor=int(respuesta["value"])
                elif respuesta["value"]=="-1":
                    respuesta_obj.valor=-1
            respuesta_obj.save()

        return ies_data_responce(ies)

class OtrosIES(views.APIView):
    permission_classes = (permissions.AllowAny, )
    def post(self, request, *args, **kwargs):
        from pprint import pprint 
        pprint(request.data)
        ies_id=request.data.get('ies_id')
        if not ies_id:
            return Response({"status":False}, status=status.HTTP_400_BAD_REQUEST)
        try:
            ies=IES.objects.get(id=ies_id)
        except Exception as e:
            ies=False
            return Response({"status":False, "error":"id de IES no encontrado"},
                status=status.HTTP_400_BAD_REQUEST)
        datos=IESSerializer(ies)

        data=datos.data
        data["status"]=True
        return Response(data, status=status.HTTP_200_OK)

class OtrosAnswersIES(views.APIView):
    permission_classes = (permissions.AllowAny, )
    def post(self, request, *args, **kwargs):
        ies_id=request.data.get('ies_id')
        lactancia=request.data.get("lactancia")
        guarderias=request.data.get("guarderias")
        licencias_paternidad=request.data.get("licencias_paternidad")
        licencias_maternidad=request.data.get("licencias_maternidad")
        pob_lgbtiq_est=request.data.get("pob_lgbtiq_est")
        pob_lgbtiq_acad=request.data.get("pob_lgbtiq_acad")
        pob_lgbtiq_admin=request.data.get("pob_lgbtiq_admin")
        quejas_genero_hom=request.data.get("quejas_genero_hom")
        quejas_genero_muj=request.data.get("quejas_genero_muj")
        quejas_genero_lgbtiq=request.data.get("quejas_genero_lgbtiq")
        prev_violencia_muj=request.data.get("prev_violencia_muj")
        prev_violencia_hom=request.data.get("prev_violencia_hom")
        prev_violencia_lgbtiq=request.data.get("prev_violencia_lgbtiq")
        hombres_directores=request.data.get("hombres_directores")
        mujeres_directoras=request.data.get("mujeres_directoras")
        if not ies_id:
            return Response({"status":False}, status=status.HTTP_400_BAD_REQUEST)
        try:
            ies=IES.objects.get(id=ies_id)
        except Exception as e:
            ies=False
            return Response({"status":False, "error":"id de IES no encontrado"},
                status=status.HTTP_400_BAD_REQUEST)


        ies.lactancia=lactancia if lactancia else ies.lactancia
        ies.guarderias=guarderias if guarderias else ies.guarderias
        ies.licencias_paternidad=licencias_paternidad if licencias_paternidad else ies.licencias_paternidad
        ies.licencias_maternidad=licencias_maternidad if licencias_maternidad else ies.licencias_maternidad
        ies.pob_lgbtiq_est=pob_lgbtiq_est if pob_lgbtiq_est else ies.pob_lgbtiq_est
        ies.pob_lgbtiq_acad=pob_lgbtiq_acad if pob_lgbtiq_acad else ies.pob_lgbtiq_acad
        ies.pob_lgbtiq_admin=pob_lgbtiq_admin if pob_lgbtiq_admin else ies.pob_lgbtiq_admin
        ies.quejas_genero_hom=quejas_genero_hom if quejas_genero_hom else ies.quejas_genero_hom
        ies.quejas_genero_muj=quejas_genero_muj if quejas_genero_muj else ies.quejas_genero_muj
        ies.quejas_genero_lgbtiq=quejas_genero_lgbtiq if quejas_genero_lgbtiq else ies.quejas_genero_lgbtiq
        ies.prev_violencia_muj=prev_violencia_muj if prev_violencia_muj else ies.prev_violencia_muj
        ies.prev_violencia_hom=prev_violencia_hom if prev_violencia_hom else ies.prev_violencia_hom
        ies.prev_violencia_lgbtiq=prev_violencia_lgbtiq if prev_violencia_lgbtiq else ies.prev_violencia_lgbtiq
        ies.hombres_directores=hombres_directores if hombres_directores else ies.hombres_directores
        ies.mujeres_directoras=mujeres_directoras if mujeres_directoras else ies.mujeres_directoras

        ies.save()
        return ies_data_responce(ies)


"""
u'ent_acad': [u'34'],
u'planes_est': [u'13'],
u'organigrama': [<TemporaryUploadedFile: Verbos Ingleses.pdf (application/pdf)>],
u'logo': [<TemporaryUploadedFile: Plan te entrenamiento_2.jpg (image/jpeg)>],
u'nombre': [u'Nombre'],
u'dep_admin': [u'53'],
u'siglas': [u'nmb']}
"""





class JsonViewer(views.APIView):
    permission_classes = (permissions.AllowAny, )
    def post(self, request, *args, **kwargs):
        from pprint import pprint

        print ""
        print "*************************Request Data************************"
        pprint(request.data)
        for data in request.data:
            print data
            print request.data[data]
            print type(request.data[data])
            print ""

        print ""
        print "*************************Request File************************"
        pprint(request.FILES)

        for file in request.FILES:
            print file
            print request.FILES[file]
            print type(request.FILES[file])
            print ""
        return Response({"status":True}, status=status.HTTP_200_OK)


class MultiFiles(views.APIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = FileSerializer


    def post(self, request, *args, **kwargs):
        images=request.FILES.get('images')
        from api.models import Archivo, IES, Pregunta
        #ies=IES.objects.get(id=1)
        #pregunta=Pregunta.objects.get(id=1)
        for file in request.FILES:
            print file
            obj_=Archivo.objects.create(doc_respaldo=request.FILES.get(file), ies_id=1, pregunta_id=1)
            print obj_

        #return Response({"valid":False}, status=status.HTTP_400_BAD_REQUEST)
        return Response({"status":True}, status=status.HTTP_200_OK)




class IsOwnerOrSuper(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True
        return obj.owner == request.user

class ArchivoViewSet(viewsets.ModelViewSet):
    queryset = Archivo.objects.all()
    serializer_class = ArchivoSerializer
    permission_classes = (permissions.AllowAny, )