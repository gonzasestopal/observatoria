# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-06-25 16:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='ies',
            name='dep_admin',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='ies',
            name='ent_acad',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='ies',
            name='descripcion',
            field=models.TextField(blank=True, null=True),
        ),
    ]
