from django.http import JsonResponse, HttpResponse
import json
from api.models import Respuesta, IES, Eje, Indicador, Poblacion, Segmento, Pregunta
from django.db.models import Avg, Sum, Count, F, Q
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth import login as signin
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from rest_framework.authtoken.models import Token
from django.conf import settings
m_host=settings.DOMINIO+settings.MEDIA_URL

@csrf_exempt
@require_http_methods(['POST'])
def login(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        username = data['username']
        password = data['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            signin(request, user)
            req_={'user': user.username}
            try:
                ies=IES.objects.get(user=user)
                req_["ies_id"]=ies.id
                token, created = Token.objects.get_or_create(user=user)
                data['token'] = token
            except Exception as e:
                pass

            return JsonResponse(req_, status=200)
        return HttpResponse(status=404)

def ejes(request):
    ejes = Eje.objects.values('nombre_corto')
    
    instituciones = Respuesta.objects.values('pregunta__indicador__eje__nombre', 'ies__nombre') \
            .annotate(promedio=Avg('valor'))\
            .values('ies__siglas', 'ies__nombre', 'pregunta__indicador__eje__nombre_corto', \
                'promedio','pregunta__indicador__eje__color')\
            .order_by('ies__siglas', 'pregunta__indicador__eje__id')
            
    ies = []
    idx_ies = -1
    sigla = None
    for institucion in instituciones:
        if not sigla or sigla != institucion['ies__siglas']:
            ies.append({'siglas': institucion['ies__siglas'], 'nombre' : institucion['ies__nombre'], 'elementos': []})
            idx_ies += 1
        if (institucion['pregunta__indicador__eje__nombre_corto'] != 'Igualdad'):
            ies[idx_ies]['elementos'].append({'nombre_corto': institucion['pregunta__indicador__eje__nombre_corto'], 
                                         'color': institucion['indicador__eje__color'],
                                         'valor': institucion['promedio']})
            sigla = institucion['ies__siglas']
    
    if request.GET.get('q'):
        ies = [x for x in ies if request.GET.get('q').lower() in x['nombre'].lower() or
                request.GET.get('q').upper() in x['siglas']]
    
    data = {'items': list(ies), 'elementos': list(ejes)}
    return JsonResponse(data)


def institucion(request, siglas):
    institucion_ = IES.objects.get(siglas=siglas.upper())
    poblaciones = institucion_.poblaciones()
    poblaciones_genero = []
    for poblacion in poblaciones:
        poblaciones_genero.append({'orden_grupo':poblacion["orden_grupo"],
                                   'nombre_grupo':poblacion["nombre_grupo"],
                                   'sexo': "mujeres",
                                   'valor': poblacion["mujeres"],
                                   'tipo':poblacion["tipo"],
                                   'mekko':poblacion["mekko"],
                                   'mekko':poblacion["mekko"],
                                   'segmento__descripcion':poblacion["segmento__descripcion"],
                                   'segmento__nombre':poblacion["segmento__nombre"] })
        poblaciones_genero.append({'orden_grupo':poblacion["orden_grupo"],
                                   'nombre_grupo':poblacion["nombre_grupo"],
                                   'sexo': "hombres",
                                   'valor': poblacion["hombres"],
                                   'tipo':poblacion["tipo"],
                                   'mekko':poblacion["mekko"],
                                   'mekko':poblacion["mekko"],
                                   'segmento__descripcion':poblacion["segmento__descripcion"],
                                   'segmento__nombre':poblacion["segmento__nombre"] })

    data = {'items': {},
            'respuestas': list(institucion_.ejes()), 
            'nombre': institucion_.nombre, 
            'siglas':institucion_.siglas,
            #'logo':request.build_absolute_uri(institucion_.logo_img),
            "logo":"%s%s"%(m_host, institucion_.logo) if institucion_.logo else False,
            'poblaciones':list(institucion_.poblaciones()),
            'poblaciones_genero':list(poblaciones_genero)}

    print institucion_.poblaciones()
    return JsonResponse(data)
    """ "organigrama": {"url":"%s%s"%(m_host, ies.organigrama.name),
                     "name":os.path.basename(ies.organigrama.name)
                    } if ies.organigrama else False
    """

def lista_ies(request):
    ies = IES.objects.all().values('nombre','siglas')
    return JsonResponse({'items':list(ies)})


def ejes_promedio(request, siglas):
    promedio = Respuesta.objects.filter(ies__siglas=siglas.upper()) \
        .values('indicador__eje') \
        .annotate(promedio=Avg('valor')) \
        .values('promedio', 'indicador__eje__color', 'indicador__eje__nombre_corto')

    data = {'items': list(promedio) }
    return JsonResponse(data)

def eje(request, siglas, nombre_corto):
    
    institucion = IES.objects.get(siglas=siglas.upper())

    ejes = Respuesta.objects.filter(ies=institucion, 
                        indicador__eje__nombre_corto=nombre_corto.capitalize()) \
                    .values('indicador__nombre', 'valor', 'indicador__color')
    
    promedio = Respuesta.objects.filter(ies=institucion, 
                        indicador__eje__nombre_corto=nombre_corto.capitalize()) \
                    .values('indicador__eje') \
                    .annotate(promedio=Avg('valor'))    
    data = {'items': list(ejes), 'promedio': promedio[0]['promedio']}
    return JsonResponse(data)

def eje2(request, nombre_corto):
    eje = Eje.objects.filter(slug=nombre_corto).values('nombre', 'nombre_corto', 
                                        'descripcion', 'logo', 'descripcion')

    data = {'items': list(eje)[0]}    
    return JsonResponse(data)

def home(request):
    all_preguntas, ies, instancias = get_basic_info()
    instituciones_q = get_all_instituciones()
    ejes = Eje.objects.all().values('id', 'nombre', 'nombre_corto', 'slug', 
        'color').exclude(slug="igualdad").order_by('id')
    indicadores = get_all_indicadores()
    all_responses = get_all_responses()
    from datetime import datetime
    for institucion in instituciones_q:
        institucion, responses_ies = build_institucion(institucion, all_responses)
        valor_ies = 0
        for eje in ejes:
            indicadores_eje = indicadores.filter(eje__slug=eje["slug"])
            institucion, eje_value, for_append = calculate_indicadores(indicadores_eje, all_preguntas, instancias, institucion, all_responses, True)
            valor_ies += eje_value
            institucion["elementos"].append({"nombre":eje["nombre_corto"],"valor":eje_value,"color":eje["color"]})
        if valor_ies >0:
            ies.append(institucion)
    #data = {'items': list(ies), 'elementos': list(ejes)}
    data = { 'elementos': list(ejes), 'items': list(ies)}
    return JsonResponse(data)

def indicadores(request, eje):
    all_preguntas, ies, instancias = get_basic_info()
    instituciones_q = get_all_instituciones()
    indicadores_eje = get_all_indicadores().filter(eje__slug=eje)
    all_responses = get_all_responses().filter(pregunta__indicador__eje__slug=eje)
    try:
        color_eje = Eje.objects.get(slug=eje).color
    except:
        color_eje = "#FFFFFF"
    for institucion in instituciones_q:
        institucion, responses_ies = build_institucion(institucion, all_responses)
        institucion["elementos"].append({"nombre":eje,"valor":0,"color":color_eje})
        institucion, eje_value, for_append = calculate_indicadores(indicadores_eje, 
                    all_preguntas, instancias, institucion, all_responses, False)
        institucion["elementos"][0]["valor"] = eje_value
        if eje_value > 0:
            ies.append(institucion)
    data = {'items': list(ies), 'elementos': list(indicadores_eje)}
    return JsonResponse(data)

def ejes2(request, siglas):
    all_preguntas = Pregunta.objects.filter(institut=True).values("indicador__id")
    ies = []
    instancias = ["academia", "administ", "planes"]

    ejes = Eje.objects.all().values('id', 'nombre', 'nombre_corto', 'slug', 
        'color').exclude(slug="igualdad").order_by('id')
    all_preguntas, ies, instancias = get_basic_info()
    all_indicadores = get_all_indicadores()
    all_responses = get_all_responses()
    #instituciones_q = get_all_instituciones().exclude(ies__dep_admin__isnull=True)
    institucion = get_all_instituciones().filter(ies__siglas=siglas)[0]
    institucion, responses_ies = build_institucion(institucion, all_responses)
    institucion["elementos"] = []
    alt_values = []
    for eje in ejes:
        for_append = {}
        indicadores_eje = all_indicadores.filter(eje__slug=eje["slug"])
        institucion, eje_value, for_append = calculate_indicadores(indicadores_eje, 
                        all_preguntas, instancias, institucion, all_responses, True)
        try:
            color_eje = Eje.objects.get(slug=eje["slug"]).color
        except:
            color_eje = "#FFFFFF"
        for app in for_append:
            app["eje"] = eje["slug"]
            alt_values.append(app)
        #eje.append({"nombre":eje,"valor":eje_value,"color":color_eje})
        eje["nombre"] = eje["nombre"]
        eje["nombre_corto"] = eje["nombre_corto"]
        eje["valor"] = eje_value 
        eje["color"] = color_eje
        eje["valores"] = for_append
        #institucion["elementos"].append(for_append)
    data = {'items': list(ejes), 'ejes': list(ejes), 'alt' :list(alt_values)}

    valores = Respuesta.objects.filter(ies__siglas=siglas) \
        .values('pregunta__indicador__eje', 'pregunta__indicador__nombre', 
                'pregunta__indicador__color', 'valor') \
        .order_by('pregunta__indicador__eje')

    return JsonResponse(data)


def get_basic_info():
    all_preguntas = Pregunta.objects.filter(institut=True).values("indicador__id")
    ies = []
    instancias = ["academia", "administ", "planes"]
    return all_preguntas, ies, instancias

def get_all_instituciones():
    return Respuesta.objects.exclude(ies__dep_admin__isnull=True)\
                .values('ies__siglas', 'ies__nombre','ies__ent_acad', 
                    'ies__dep_admin', 'ies__planes_est')\
                .annotate(academia2 = F('ies__ent_acad'), 
                    administ2 = F('ies__dep_admin'), 
                    planes2 = F('ies__planes_est'))\
                .distinct()

def get_all_indicadores():
    return Indicador.objects.all()\
        .values('id', 'nombre','academia', 'administ', 'planes', 
                'transver_value', 'instit_value', 'color', 'eje__slug')\
        .distinct()\
        .order_by('id')


def get_all_responses():
    return Respuesta.objects.all()\
        .values('valor','academia', 'administ', 'planes', 
            'ies__nombre', 'ies__siglas',
            'pregunta__institut', 
            'pregunta__indicador__academia', 'pregunta__indicador__administ', 
            'pregunta__indicador__planes', 'pregunta__indicador__id', 
            'pregunta__indicador__nombre', 'pregunta__indicador__color', 
            'pregunta__indicador__transver_value', 'pregunta__indicador__instit_value', 
            'pregunta__indicador__eje__nombre_corto', 'pregunta__indicador__eje__color')\
        .order_by('ies','pregunta__indicador__id')


def build_institucion(institucion, all_responses):
    responses_ies = all_responses.filter(ies__siglas=institucion["ies__siglas"])
    institucion["elementos"] = []
    institucion["siglas"] = institucion["ies__siglas"]
    institucion["nombre"] = institucion["ies__nombre"]
    return institucion, responses_ies

def calculate_indicadores(indicadores_eje, all_preguntas, instancias, institucion, 
                            all_responses, vista_general):
    valor_eje = 0
    final_append = []
    for indicador in indicadores_eje:
        preguntas_instit = all_preguntas.filter(indicador__id=indicador["id"]).count()
        instancias_success = 0
        total_instancias = 0
        for instancia in instancias:
            if  indicador[instancia] and institucion[instancia+"2"]:
                try:
                    instancias_success += all_responses\
                                    .get(ies__siglas=institucion["ies__siglas"], 
                                    pregunta__indicador_id=indicador["id"], 
                                    pregunta__institut="False")[instancia]
                except:
                    pass
                total_instancias += institucion[instancia+"2"]
        try:
            suma_institut = all_responses\
                                .filter(ies__siglas=institucion["ies__siglas"], 
                                    pregunta__indicador_id=indicador["id"],
                                    valor__isnull=False)\
                                .aggregate(total=Sum("valor"))["total"]
        except:
            suma_institut = 0
        valor_instit = 0
        valor_transver = 0
        if total_instancias > 0:
            valor_transver = (instancias_success / float(total_instancias))  * indicador["transver_value"] 
        if preguntas_instit > 0 and suma_institut >0:
            valor_instit = (suma_institut / float(preguntas_instit)) * indicador["instit_value"]
        valor_indicador = round(valor_transver + valor_instit, 1)
        if valor_indicador > 0:
            print institucion["ies__siglas"]
            print suma_institut
            print valor_instit
        indicador["valor"] = valor_indicador
        valor_eje += valor_indicador
        final_append.append({"nombre":indicador["nombre"], 
                            "valor":valor_indicador, 
                            "color":indicador["color"]})
        if not vista_general:
            institucion["elementos"].append({"nombre":indicador["nombre"], 
                                            "valor":valor_indicador, 
                                            "color":indicador["color"]})
    eje_value = round(valor_eje / float(len(indicadores_eje)),1)
    return institucion, eje_value, final_append


##Debemos eliminar esto que hipersobra
def areas(request, siglas, tipo):
    institucion = IES.objects.get(siglas=siglas.upper())

    areas = Poblacion.objects.filter(institucion=institucion, 
                                    segmento__tipo=tipo.upper())\
                            .values('segmento__nombre', 'segmento__genero', 'valor')\
                            .order_by('-segmento__genero')

    data = {'items': list(areas)}


    return JsonResponse(data)

def segmento_estudiantil(request):
    
    segmentos = Poblacion.objects.filter(segmento__tipo='EST').values('segmento__nombre').annotate(total=Sum('valor')).values('segmento__nombre','segmento__descripcion','total')
        
    data = {'items': list(segmentos)}
    
    return JsonResponse(data)

def poblacion(request):
    
    estudiantes = Poblacion.objects.filter(segmento__tipo="EST").values('segmento__genero').annotate(suma=Sum('valor')).order_by('-segmento__genero')
    
    administrativos = Poblacion.objects.filter(segmento__tipo="ADM").values('segmento__genero').annotate(suma=Sum('valor')).order_by('-segmento__genero')
    
    academicos = Poblacion.objects.filter(segmento__tipo="ACD").values('segmento__nombre', 'valor')

    total_estudiantes = Poblacion.objects.filter(segmento__tipo="EST").aggregate(total=Sum('valor'))
    
    total_administrativos = Poblacion.objects.filter(segmento__tipo="ADM").aggregate(total=Sum('valor'))
    
    total_academicos = Poblacion.objects.filter(segmento__tipo="ACD").values('segmento__nombre').annotate(total=Sum('valor'))

    promedio_estudiantes = []
    promedio_administrativos = []
    promedio_academicos = []
    
    group_by_segmento = {}
    for item in total_academicos:
        group_by_segmento.setdefault(item['segmento__nombre'], []).append(item)
    
    for estudiante in estudiantes:
        promedio_estudiantes.append("{0:0.1f}".format(100 * float(estudiante['suma']) / float(total_estudiantes['total'])))

    for administrativo in administrativos:
        promedio_administrativos.append("{0:0.1f}".format(100 * float(administrativo['suma']) / float(total_administrativos['total'])))

    # for academico in academicos:
    #     promedio_academicos.append("{0:0.1f}".format(100 * float(academico['suma']) / float(total_academicos['total'])))

    res = {}
    for item in academicos:
        res.setdefault(item['segmento__nombre'],  []).append(item)
    
    for key, value in res.iteritems():
        for v in value:
            aver = v['segmento__nombre']
            v['promedio'] = "{0:0.1f}".format(100 * float(v['valor']) / float(group_by_segmento[aver][0]['total']))
    
        
    data = {'items': {'estudiantes': promedio_estudiantes, 'administrativos': promedio_administrativos, 'academicos': res}}
    
    return JsonResponse(data)    

@csrf_exempt
def dashboard_indicadores(request, user):
    
    user = user.upper()
    
    if request.method == 'POST':
        data =  json.loads(request.body)
        indicador = data['indicador']['indicador']
        valor = data['valor']
        
        respuesta = Respuesta.objects.get(indicador__respuesta__ies__siglas=user, indicador=indicador)
        respuesta.valor = valor
        respuesta.save()
        return JsonResponse({'respuesta': data})
    
    ejes = Eje.objects.filter(indicador__respuesta__ies__siglas=user)\
            .values('nombre', 'indicador__nombre', 'indicador', 
                'indicador__pregunta').exclude(nombre='Igualdad')
    print ejes

    res = {}
    for item in ejes:
        res.setdefault(item['nombre'],  []).append(item)

        
    for key, value in res.iteritems():
        for v in value:
            #if v['nombre'] == key:
            #    v['opciones'] = list(OpcionesRespuesta.objects.filter(indicador=v['indicador']).values('texto', 'valor'))
            v['selected'] = Respuesta.objects.filter(indicador__respuesta__ies__siglas=user, indicador=v['indicador'])[0].valor
    
    data = {'respuestas': res}
    
    return JsonResponse(data)


def dashboard_poblacion(request, user):
    
    user = user.upper()

    segmentos= Segmento.objects.filter(poblacion__institucion__siglas=user)\
            .values('nombre','tipo','descripcion','poblacion','genero',
                'subpoblacion').order_by('-tipo')
    
    res = {}
    for item in segmentos:
        res.setdefault(item['tipo'],  []).append(item)

    """for key, value in res.iteritems():
        for v in value:
            if v['nombre'] == key:
                v['opciones'] = list(OpcionesRespuesta.objects.filter(indicador=v['indicador']).values('texto', 'valor'))
            v['selected'] = Respuesta.objects.filter(indicador__respuesta__ies__siglas=user, indicador=v['indicador'])[0].valor"""
 
    data = {'poblaciones': res.items()}
    
    return JsonResponse(data)

def poblaciones(request):
    tipos = [{'nombre': u'Poblacion Estudiantil'}, {'nombre': u'Poblacion Administrativa'}, {'nombre': u'Poblacion Academica'}]

    ies = Poblacion.objects.filter(Q(mujeres__gt=0) | Q(hombres__gt=0))\
                    .values('institucion__siglas','institucion__id','institucion__nombre')\
                    .annotate(siglas = F('institucion__siglas'),tot_mujeres=Sum('mujeres'))\
                    .filter(tot_mujeres__gt=0)\
                    .order_by('institucion__siglas')

    ies_ids = []
    for ie in ies:
        ies_ids.append(ie["institucion__id"])
    print ies_ids

    poblaciones = Poblacion.objects.filter(institucion__id__in=ies_ids)\
                .values('nombre','hombres','mujeres','institucion__siglas','segmento__grupo_segmento__tipo')\
                .order_by('institucion__siglas','-segmento__grupo_segmento__tipo')

    data = {'items': list(ies), 'elementos': tipos, 'poblaciones': list(poblaciones)}
    return JsonResponse(data)


def poblaciones2(request):
    tipos = {'ACD': 'Academico', 'ADM':'Administrativo','EST':'Estudiantil'}
    
    tipos2 = []
    for x in Poblacion.objects.all():
        if {'nombre': x.segmento.get_tipo_display()} not in tipos2:
            tipos2.append({'nombre': x.segmento.get_tipo_display()})     
        
    elementos = Poblacion.objects.values('segmento__tipo','institucion') \
                    .annotate(promedio=Sum('valor')) \
                    .values('institucion__siglas', 'institucion__nombre', 'segmento__tipo', 'segmento__genero',  'promedio') \
                    .order_by('institucion__siglas')

            
    ies = []
    idx_ies = -1

    sigla = None

    for elemento in elementos:
        if not sigla or sigla != elemento['institucion__siglas']:
            ies.append({'siglas': elemento['institucion__siglas'], 'nombre' : elemento['institucion__nombre'], 'elementos2': []})
            idx_ies += 1
        ies[idx_ies]['elementos2'].append({'tipo': elemento['segmento__tipo'],
                                     'genero': elemento['segmento__genero'],
                                     'promedio': elemento['promedio']})
        sigla = elemento['institucion__siglas']
    
    for ie in ies:
        idx_ele = -1
        last_type = ''
        ie['elementos'] = []
        for elemento in ie['elementos2']:
            
            if not last_type or last_type != elemento['tipo']:
                idx_ele += 1
                ie['elementos'].append({elemento['genero']: elemento['promedio'],'tipo' : tipos[elemento['tipo']]})  

            else:
                # ie['elementos2'][idx_ele][elemento['genero']] = elemento['promedio']
                ie['elementos'][idx_ele]['femin'] = elemento['promedio'] / float(ie['elementos'][idx_ele]["H"] + elemento['promedio'])
                
            last_type = elemento['tipo']    
            
        
        
    
    if request.GET.get('q'):
        ies = [x for x in ies if request.GET.get('q').lower() in x['nombre'].lower() or
                request.GET.get('q').upper() in x['siglas']]
    
    data = {'items': list(ies), 'elementos': tipos2}
    return JsonResponse(data)
