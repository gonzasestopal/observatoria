# encoding:utf-8

"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from core.views import ejes, institucion, eje, eje2, indicadores, areas, login, ejes2, \
            segmento_estudiantil, poblacion, dashboard_indicadores, dashboard_poblacion, poblaciones, \
            ejes_promedio, home, lista_ies
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve
admin.autodiscover()
admin.site.site_header = u'Administración ONIGIES'
admin.site.site_title ="Admin"
admin.site.index_title="Tablas"

urlpatterns = [
    url(r'^api/', include('api.urls')),
    url(r'^user/login/$', login, name='login'),
    url(r'^admin/', admin.site.urls),
    url(r'^$', home),
    url(r'^segmentos/$', segmento_estudiantil),
    url(r'^all_ies/$', lista_ies),
    url(r'^poblacion/$', poblacion),
    url(r'^poblaciones/$', poblaciones),    
    url(r'^dashboard_indicadores/(?P<user>\w+)/$', dashboard_indicadores),
    url(r'^dashboard_poblacion/(?P<user>\w+)/$', dashboard_poblacion),
    url(r'^(?P<siglas>\w+)/$', institucion),
    url(r'^ies/(?P<siglas>\w+)/(?P<nombre_corto>\w+)/$', eje),
    url(r'^ies2/(?P<siglas>\w+)/$', ejes_promedio),
    url(r'^eje/(?P<nombre_corto>[\w ]+)/$', eje2),
    url(r'^indicadores/(?P<eje>[\w ]+)/$', indicadores),
    url(r'^areas/(?P<siglas>\w+)/(?P<tipo>\w+)/$', areas),
    url(r'^ejes/(?P<siglas>\w+)/$', ejes2),

] 

if settings.DEBUG: 
    urlpatterns.append(url(r'^static/(?P<path>.*)$', serve, {
        'document_root': settings.STATIC_ROOT,
        }))
    urlpatterns.append(url(r'^media/(?P<path>.*)$', serve, {
        'document_root': settings.MEDIA_ROOT,
        }))