# Observatoria

UNAM Project

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

```
sudo apt install nodejs npm
sudo npm install -g @angular/cli
sudo apt-get install python3-pip
sudo pip3 install virtualenv
```

### Installing

Install development tools

```
git clone https://gonzasestopal@bitbucket.org/gonzasestopal/observatoria.git
virtualenv venv
cd observatoria
pip install -r requirements.txt
npm install
```

Then run development servers

```
python manage.py runserver
ng serve --watch
```

You should be able to open the app at localhost:4200

## Deployment

Ask for credentials to gonzasestopal@gmail.com

```

ssh -i key.pem ubuntu@server
git pull
npm run clean:dist
npm run webpack -- --config config/webpack.prod.js  --progress --profile --bail
sudo systemctl restart gunicorn

```

## Built With

* [Angular](https://angular.io/) - Frontend Framework
* [Django](https://www.djangoproject.com/) - API

## Contributing

Please fork or submit pull request.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Gonza Sestopal** - *Initial work* - [gonzasestopal](https://github.com/gonzasestopal)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
